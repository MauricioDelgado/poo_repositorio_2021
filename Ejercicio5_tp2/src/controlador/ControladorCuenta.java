package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;

import modelo.Banco;
import modelo.Persona;
import vista.Ventana1;

public class ControladorCuenta implements ActionListener{
// ATRIBUTOS
	private Ventana1 vista; 
	private Banco banco;
	
// CONSTRUCTOR	
	public ControladorCuenta() {
		super();
		this.vista = new Ventana1(this);
		this.banco = new Banco();
		this.vista.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// aca se ejecuta codigo cuando se da aceptar o guardar
		if (e.getSource().equals(getVista().getBtnGuardar())) {
			ArrayList<Persona>clientes =new ArrayList<Persona>();
			getBanco().leerArchivo(clientes);
			
			String nombre = getVista().getTxtNombre().getText();
			String genero = getVista().getTxtGenero().getText();
			String email = getVista().getTxtEmail().getText();
			String contrasenia = getVista().getTxtContrasenia().getText();
			String fechaNacimiento = getVista().getTxtFecha_nacimiento().getText();
			String saldo = getVista().getTextSaldo().getText();
			double saldo2 = Double.parseDouble(saldo);
			getBanco().agregarCuenta(new Persona(nombre, genero, email,LocalDate.parse(fechaNacimiento) , contrasenia), saldo2);
			//getBanco().listaCompleta();
			//getBanco().listarClientes();
			clientes.add((new Persona(nombre, genero, email,LocalDate.parse(fechaNacimiento) , contrasenia)));
			
			//lamado al modelo getBanco.guardarCuenta
			
			getBanco().cargarArchivo(clientes);
			
		}
	}
	
// METODOS
	public Ventana1 getVista() {
		return vista;
	}

	public void setVista(Ventana1 vista) {
		this.vista = vista;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}


	
	
	
	
	
	
	
}
// ActionListener se usa para botones
// implements es para interfaces
// la vista necesita instancia del controlador dentro del new ventan1(instancia) para asociar botones al listener