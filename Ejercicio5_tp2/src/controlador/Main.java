package controlador;

import java.awt.EventQueue;

import vista.Ventana1;

public class Main {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorCuenta();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
