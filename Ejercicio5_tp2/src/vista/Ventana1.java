package vista;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.SystemColor;
import java.awt.Color;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import controlador.ControladorCuenta;

import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ventana1 extends JFrame {

	private JPanel contentPane;
	private JButton btnGuardar;
	private JTextField txtNombre;
	private JTextField txtGenero;
	private JTextField txtEmail;
	private JTextField txtContrasenia;
	private JTextField txtFecha_nacimiento;
	private ControladorCuenta controlador;
	private JTextField textSaldo;
	
	

	/**
	 * Create the frame.
	 */
	public Ventana1(ControladorCuenta controlador) {
		this.setControlador(controlador);
		setForeground(new Color(0, 128, 128));
		setBackground(new Color(0, 128, 128));
		setTitle("ventana1");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 444, 319);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitulo = new JLabel("NUEVA CUENTA");
		lblTitulo.setFont(new Font("Arial Black", Font.BOLD, 14));
		lblTitulo.setBounds(138, 22, 137, 24);
		contentPane.add(lblTitulo);
		
		JLabel lblNombre = new JLabel("NOMBRE:");
		lblNombre.setForeground(new Color(255, 255, 255));
		lblNombre.setBounds(10, 79, 71, 24);
		contentPane.add(lblNombre);
		
		JLabel lblGenero = new JLabel("GENERO");
		lblGenero.setForeground(new Color(255, 255, 255));
		lblGenero.setBounds(10, 102, 50, 24);
		contentPane.add(lblGenero);
		
		JLabel lblEmail = new JLabel("EMAIL:");
		lblEmail.setForeground(new Color(255, 255, 255));
		lblEmail.setBounds(10, 126, 83, 24);
		contentPane.add(lblEmail);
		
		JLabel lblContrasenia = new JLabel("CONTRASE\u00D1A:");
		lblContrasenia.setForeground(new Color(255, 255, 255));
		lblContrasenia.setBounds(10, 151, 83, 24);
		contentPane.add(lblContrasenia);
		
		JLabel lblFecha_nacimiento = new JLabel("FECHA DE NACIMIENTO:");
		lblFecha_nacimiento.setForeground(new Color(255, 255, 255));
		lblFecha_nacimiento.setBounds(10, 175, 149, 24);
		contentPane.add(lblFecha_nacimiento);
		
		txtNombre = new JTextField();
		lblNombre.setLabelFor(txtNombre);
		txtNombre.setBounds(193, 81, 225, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtGenero = new JTextField();
		lblGenero.setLabelFor(txtGenero);
		txtGenero.setToolTipText("M/F/X");
		txtGenero.setColumns(10);
		txtGenero.setBounds(193, 104, 225, 20);
		contentPane.add(txtGenero);
		
		txtEmail = new JTextField();
		lblEmail.setLabelFor(txtEmail);
		txtEmail.setColumns(10);
		txtEmail.setBounds(193, 128, 225, 20);
		contentPane.add(txtEmail);
		
		txtContrasenia = new JTextField();
		lblContrasenia.setLabelFor(txtContrasenia);
		txtContrasenia.setToolTipText("8 caracteres, en minuscula sin %&-;");
		txtContrasenia.setColumns(10);
		txtContrasenia.setBounds(193, 151, 225, 20);
		contentPane.add(txtContrasenia);
		
		txtFecha_nacimiento = new JTextField();
		lblFecha_nacimiento.setLabelFor(txtFecha_nacimiento);
		txtFecha_nacimiento.setToolTipText("A\u00F1o-Mes-Dia");
		txtFecha_nacimiento.setColumns(10);
		txtFecha_nacimiento.setBounds(193, 177, 225, 20);
		contentPane.add(txtFecha_nacimiento);
		
		btnGuardar = new JButton("GUARDAR");
		btnGuardar.addActionListener(getControlador());
		btnGuardar.setFont(new Font("Arial", Font.PLAIN, 10));
		btnGuardar.setBounds(170, 246, 89, 23);
		contentPane.add(btnGuardar);
		
		JLabel lblSaldo = new JLabel("SALDO:");
		lblSaldo.setForeground(Color.WHITE);
		lblSaldo.setBounds(10, 200, 149, 24);
		contentPane.add(lblSaldo);
		
		textSaldo = new JTextField();
		lblSaldo.setLabelFor(textSaldo);
		textSaldo.setToolTipText("A\u00F1o-Mes-Dia");
		textSaldo.setColumns(10);
		textSaldo.setBounds(193, 202, 225, 20);
		contentPane.add(textSaldo);
	}



	public ControladorCuenta getControlador() {
		return controlador;
	}



	public void setControlador(ControladorCuenta controlador) {
		this.controlador = controlador;
	}



	public JButton getBtnGuardar() {
		return btnGuardar;
	}



	public JTextField getTxtNombre() {
		return txtNombre;
	}



	public JTextField getTxtGenero() {
		return txtGenero;
	}



	public JTextField getTxtEmail() {
		return txtEmail;
	}



	public JTextField getTxtContrasenia() {
		return txtContrasenia;
	}



	public JTextField getTxtFecha_nacimiento() {
		return txtFecha_nacimiento;
	}



	public JTextField getTextSaldo() {
		return textSaldo;
	}
	
	
}
