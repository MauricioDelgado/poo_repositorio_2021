package modelo;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

public class Banco {
// ATRIBUTOS
	private String nombre; 
	private HashMap<Persona, Double> cuentas;
	

	public Banco() {
		this("", null); 
	}
	
	
// CONSTRUCTORES
	public Banco(String nombre, HashMap<Persona, Double> cuentas) {
		this.setNombre(nombre);
		//this.setCuentas(cuentas);
		this.cuentas = new HashMap<>();
		
		//crear archivo
	}

// METODOS
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public HashMap<Persona, Double> getCuenta() {
		return cuentas;
	}

	public void setCuentas(HashMap<Persona, Double> cuentas) {
		this.cuentas = cuentas;
	}
	
// METODOS PROPIOS	
	public Boolean agregarCuenta(Persona persona, Double saldo) {
		if(!cuentas.containsKey(persona)) {
			cuentas.put(persona, saldo);
			return true;
		}else {
			return false;
		}
		//actualizar archivo
	}
	
	public Boolean eliminarCuenta(Persona persona) {
		if(cuentas.containsKey(persona)) {
			cuentas.remove(persona);
			return true;
		}else {
			return false;
		}
		
	}
	
	public void listarClientes() {
		Iterator cliente = cuentas.keySet().iterator();
		while (cliente.hasNext()) {
			Persona nombre = (Persona) cliente.next();
			System.out.println("Nombre Cliente: " + nombre.getNombre()+ ": saldo= " + cuentas.get(nombre));
		}
	}
	
	public void listaCompleta( ) {
		System.out.println(cuentas); //-->con esto imprime todo, datos de la persona y saldo

	}
	
	public Double getSaldo(Persona persona) {
		for(Map.Entry<Persona, Double> entrada : cuentas.entrySet()) {
			if(entrada.getKey().equals(persona))
			{
				return entrada.getValue();
			}
		}
			return -1.0; 
	}
	
	// metodo guardado
	
	public ArrayList<Persona>leerArchivo (ArrayList<Persona>datos) {
		try (Scanner input = new Scanner(Paths.get("archivos/cuentas.csv"))){
			while (input.hasNext()) {
				Persona unaPersona = new Persona(input.nextLine(), input.next(), input.nextLine(), LocalDate.parse(input.next()), input.next());
				datos.add(unaPersona); 
				agregarCuenta(unaPersona, 0.0);
			}
			}catch (IOException a) {
				a.printStackTrace();
			}
		return datos;	
	}
	
	public void cargarArchivo (ArrayList<Persona>datos) {
		try (Formatter output = new Formatter("archivos/cuentas.csv")){
			for (Persona persona : datos) {
				output.format("%s %c %s %s %s %n", persona.getNombre(), persona.getGenero(), persona.getEmail(), persona.getFecha_nacimiento(), persona.getContrasenia());
			}
		}catch (FileNotFoundException exc) {
			exc.printStackTrace(); 
		}
	}
		
		
	
	
}
