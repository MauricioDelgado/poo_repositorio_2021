	import java.io.FileNotFoundException;
	import java.util.Formatter;
	import java.util.NoSuchElementException;
	import java.util.Scanner;

	public class CrearTxt2 {	
	 public static void main(String[] args) {
			
		try(Formatter salida = new Formatter("assets/Gastos.txt")){  //crear carpeta Assets dentro del proyecto
			
			@SuppressWarnings("resource")
			Scanner entrada = new Scanner(System.in);	
			// lo que muestro para que ingrese
			System.out.printf("%s%n%s", 
					"MONTO, DESCRIPCION",
					"Ingrese Ctrl+z para finalizar.");
			
			while (entrada.hasNext()) {
				try {
				salida.format("%.2f %s %n ", entrada.nextDouble(), entrada.nextLine());  // formateo salida
				
				} catch(NoSuchElementException elementException) {
					System.err.println("VALOR INVALIDO INGRESE OTRA VEZ");
					entrada.nextLine();
				}
				System.out.print("? ");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	 }
	}

