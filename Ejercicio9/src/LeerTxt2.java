import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class LeerTxt2 {
	public static void main(String[] args) {
		try (Scanner entrada = new Scanner(Paths.get("assets/Gastos.txt"))) {
				System.out.printf("%-10s %13s %n",
				"MONTO", "DESCRIPCION");
			
				while(entrada.hasNext()) {    //mientras tenga entradas que procesar imprime
				System.out.printf("%-10.2f %-12s %n",
				entrada.nextDouble(), entrada.nextLine());
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
	
	}
}
