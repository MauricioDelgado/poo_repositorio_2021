import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class ContadorCaracteres {
	
	public static void main(String[] args) {
		Integer contador = 0; 
		
		try(Scanner entrada = new Scanner(Paths.get("assets/Gastos.txt"))) {
			
			while (entrada.hasNext()) {
				String cadena = entrada.next();
				for (int i = 0; i < cadena.length(); i++) {
					contador++;
					
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("El archivo tiene " + contador + " caracteres sin espacios");
	}
}
