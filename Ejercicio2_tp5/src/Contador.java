
public class Contador {
	private Integer contador = 0;
	
	public synchronized void incrementar() {
		contador++;
	}
	public synchronized void decrementar() {
		contador--;
	}
	public  Integer getContador() {
		return contador;
	}

}
