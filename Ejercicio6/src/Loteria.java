import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class Loteria {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		SecureRandom sran = new SecureRandom();
		//1ArrayList<Integer>num_jugador; 
		 
		int [] numeros_jugador =  new int[6];
		int [] numeros_loteria = new int[6];
		String[] posicion = new String[] {"Primer", "Segundo", "Tercer", "Cuarto", "Quinto", "Sexto"};
		Character rta; 
		int rnd;
		int numero; 
		
		
		//    ACA EMPEZAR EL LOOP DE RE INICO DE PROGRAMA
		System.out.println("¿QUIERES INICIAR JUEGO? s/n");
		rta = entrada.next().charAt(0);
		while (rta == 's') {
				// sorteo y almaceno los numeros ganadores
				for  (int j = 0; j < numeros_loteria.length; j++) {
					rnd = sran.nextInt(51);
					if ( existeEnArreglo(numeros_loteria, rnd)){
						numeros_loteria[j] = sran.nextInt(51);
					}else {
						numeros_loteria[j] = rnd; 
					}
				}
				
				int i = 0;
				// consulto numeros elegidos por usuario
				System.out.println("Ingrese 6 numeros no repetidos: ");		
				do {
					System.out.println(posicion[i] + " numero: ");
					numero = entrada.nextInt();
					
					if ( numero > 50 | existeEnArreglo(numeros_jugador, numero)) {
						System.out.println("NUMERO REPETIDO INGRESE UNO NUEVO: ");
						numeros_jugador[i] = entrada.nextInt();
					}else {
						numeros_jugador[i] = numero; 
					}
					i++;
						
				} while (i != 6); 
				
				// muestro numeros elegidos por usuario
				Arrays.sort(numeros_jugador);
				System.out.print("NUMEROS ELEGIDOS: ");
				for (int num : numeros_jugador) {
					System.out.print(num + " ");
				}
				System.out.println(" ");
				// muestro numeros ganadores de loteria
				Arrays.sort(numeros_loteria);
				System.out.print("NUMEROS GANADORES: ");
				for (int num : numeros_loteria) {
					System.out.print(num + " ");
				}
				
				// hago la comparacion de las 2 listas
				Integer cont = 0; 
				for (int k = 0; k < numeros_jugador.length; k++) {
					if (existeEnArreglo(numeros_loteria, numeros_jugador[k])) {
						cont++;
					}
				}
				System.out.println(" ");
				System.out.println("Hubo " + cont + " aciertos");
				
				System.out.println("¿QUIERES JUGAR NUEVAMENTE? s/n");
				rta = entrada.next().charAt(0);
	}	
		
		
		
	}


        
	public static boolean existeEnArreglo(int[] arreglo, int busqueda) {
	  for (int x = 0; x < arreglo.length; x++) {
	    if (arreglo[x] == busqueda) {
	      return true;
	    }
	  }
	  return false;
	}
}
