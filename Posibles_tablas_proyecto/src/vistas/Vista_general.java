package vistas;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.SystemColor;

public class Vista_general extends JFrame {

	private JPanel contentPane;
	private JButton btnClientes;
	private JButton btnTrabajos;
	private JButton btnMotores;
	private JButton btnPresupuestos;
	private JPanel panel_general;
	private Panel_presupuesto pp; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista_general frame = new Vista_general();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Vista_general() {
		setBackground(SystemColor.window);
		setTitle("RECTIFICADORA \"PANCHITO\" - Gestion");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1117, 663);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.window);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel_general = new JPanel();
		panel_general.setBounds(10, 45, 1081, 568);
		contentPane.add(panel_general);
		panel_general.setLayout(null);
		
		btnClientes = new JButton("CLIENTES");
		btnClientes.setBounds(10, 11, 169, 23);
		contentPane.add(btnClientes);
		
		btnTrabajos = new JButton("TRABAJOS");
		btnTrabajos.setBounds(189, 11, 169, 23);
		contentPane.add(btnTrabajos);
		
		btnMotores = new JButton("MOTORES");
		btnMotores.setBounds(368, 11, 169, 23);
		contentPane.add(btnMotores);
		
		btnPresupuestos = new JButton("PRESUPUESTOS");
		btnPresupuestos.setBounds(547, 11, 169, 23);
		contentPane.add(btnPresupuestos);
		
		//--------------------------------------------
		
		
		
	}

	public JButton getBtnNewButton() {
		return btnClientes;
	}

	public void setBtnNewButton(JButton btnNewButton) {
		this.btnClientes = btnNewButton;
	}

	public JButton getBtnTrabajos() {
		return btnTrabajos;
	}

	public void setBtnTrabajos(JButton btnTrabajos) {
		this.btnTrabajos = btnTrabajos;
	}

	public JButton getBtnMotores() {
		return btnMotores;
	}

	public void setBtnMotores(JButton btnMotores) {
		this.btnMotores = btnMotores;
	}

	public JButton getBtnPresupuestos() {
		return btnPresupuestos;
	}

	public void setBtnPresupuestos(JButton btnPresupuestos) {
		this.btnPresupuestos = btnPresupuestos;
	}

	public JPanel getPanel() {
		return panel_general;
	}

	public void setPanel(JPanel panel) {
		this.panel_general = panel;
	}

	public JButton getBtnClientes() {
		return btnClientes;
	}

	public void setBtnClientes(JButton btnClientes) {
		this.btnClientes = btnClientes;
	}

	public Panel_presupuesto getPp() {
		return pp;
	}

	public void setPp(Panel_presupuesto pp) {
		this.pp = pp;
	}
	
}
