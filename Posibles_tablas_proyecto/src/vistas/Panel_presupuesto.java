package vistas;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.SystemColor;

public class Panel_presupuesto extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JButton btnVer_detalle;
	private DefaultTableModel modeloTabla; 
	private final JScrollPane scrollPane = new JScrollPane();
	private JTable table;

	/**
	 * Create the panel.
	 */
	public Panel_presupuesto() {
		setBackground(SystemColor.window);
		setLayout(null);
		
		scrollPane.setColumnHeaderView(table);
		Panel_presupuesto panel_presupuesto = new Panel_presupuesto();
		panel_presupuesto.setBounds(10, 45, 1081, 568);
	 
		
		JLabel lblNewLabel = new JLabel("PRESUPUESTOS");
		lblNewLabel.setBounds(10, 11, 219, 28);
		add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(10, 39, 615, 20);
		add(textField);
		textField.setColumns(10);
		
		//---------------------BOTONES y LABELS---------------------------------------
		JLabel lblNewLabel_1 = new JLabel("imagen_busqueda");
		lblNewLabel_1.setBounds(635, 42, 46, 14);
		add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("boton_refresco");
		btnNewButton.setBounds(763, 0, 70, 39);
		add(btnNewButton);
		
		btnVer_detalle = new JButton("Ver detalle");
		btnVer_detalle.setBounds(716, 156, 107, 23);
		add(btnVer_detalle);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(716, 214, 107, 23);
		add(btnModificar);
		
		JButton btnCambiar_estado = new JButton("Cambiar estado");
		btnCambiar_estado.setBounds(716, 273, 107, 39);
		add(btnCambiar_estado);
		
		//--------------------------TABLA----------------------------------
		JPanel panel = new JPanel();
		panel.setBounds(0, 70, 707, 476);
		add(panel);
		panel.setLayout(null);
		scrollPane.setBounds(0, 0, 707, 476);
		panel.add(scrollPane);
		
		String header[] = {"Nro Presupuesto", "Due�o","Nro Trabajo", "Monto", "Fecha","Estado"};
		this.setModeloTabla(new DefaultTableModel(null, header));
		
		
		table = new JTable(this.getModeloTabla());
		table.setBackground(SystemColor.window);
		table.setBounds(0, 0, 532, 195);
		scrollPane.setViewportView(table);

		//----------------------------------------------	
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public JButton getBtnVer_detalle() {
		return btnVer_detalle;
	}

	public void setBtnVer_detalle(JButton btnVer_detalle) {
		this.btnVer_detalle = btnVer_detalle;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JScrollPane getScrollPane() {
		return scrollPane;
	}

	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}

	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}	
	
}
