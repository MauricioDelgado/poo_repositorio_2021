import java.time.LocalDate;

public class Persona {
// ATRIBUTOS
	private String nombre;
	private String genero;
	private String email;
	private LocalDate fecha_nacimiento;
	private String contrasenia;

// CONSTRUCTOR POR DEFECTO	
	public Persona() {
		this("","","", null, "");
	}

// CONSTRUCTOR SOBRECARGADO
	public Persona(String nombre,String genero,String email, LocalDate fecha_nacimiento, String contrasenia) {
		
		this.setNombre(nombre);
		this.setGenero(genero);
		this.setEmail(email);
		this.setFecha_nacimiento(fecha_nacimiento);
		this.setContrasenia(contrasenia);
	}
		
// METODOS
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getGenero(String genero) {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}
	
	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	@Override
	public String toString() {
		return "NOMBRE: " + nombre +  
			   "\nFECHA DE NACIMIENTO: " + fecha_nacimiento +
			   "\nGENERO: " + genero +
			   "\nEMAIL: " + email ;
	}


}
