import java.security.SecureRandom;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		SecureRandom srnd = new SecureRandom();
		
		int valor1, valor2, eleccion, perdidas, ganadas; 
		char rta; 
		
		ganadas = 0; 
		perdidas= 0; 
		
		System.out.println("Desea iniciar juego? S/N");
		rta = entrada.next().charAt(0);
		
		if (rta != 'S') {
			System.out.println("Ingrese correctamente S o N !!");
			rta = entrada.next().charAt(0);
		}
		// aca empezaria el programa 
		while (rta == 'S') {
			valor1 = srnd.nextInt(101);
			valor2 = srnd.nextInt(101);
			
			while (valor1 == valor2) {
				valor1 = srnd.nextInt(101);
				valor2 = srnd.nextInt(101);
			}
			
			do {
				System.out.println("Elija entre valor 1 o 2 (poner numero)");
				eleccion = entrada.nextInt();			
			} while (eleccion != 1 && eleccion != 2); 
				
			
			System.out.println("Valor 1: " + valor1);
			System.out.println("Valor 2: " + valor2);
			 
			if (eleccion == 1) {
				if(valor1 > valor2) {
					System.out.println("Ganaste!");
					ganadas++; 
				}else {
					System.out.println("Perdiste!");
					perdidas++;
				}	
			}else if (valor1 < valor2) {
				System.out.println("Ganaste!");
				ganadas++;
			}else {
				System.out.println("Perdiste!");
				perdidas++; 
			}
			
			do {
				System.out.println("Desea RE-iniciar juego? S/N");
				rta = entrada.next().charAt(0);
			} while (rta != 'S' &&  rta != 'N');
		
		}
		System.out.println("PARTIDAS GANADAS: " + ganadas);
		System.out.println("PARTIDAS PERDIDAS: " + perdidas);
		
		System.out.println("ADIOS!");
	}
		// termina el bucle del programa
	}

