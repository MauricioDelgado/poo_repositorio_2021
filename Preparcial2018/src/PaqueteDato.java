
public class PaqueteDato {

	private long timestamInicio; 
	private long timestamFin;
	private Usuario usuario;
	
	public PaqueteDato(Usuario usuario) {
		super();
		this.usuario = usuario;
	}

	public long getTimestamInicio() {
		return timestamInicio;
	}

	public void setTimestamInicio(long timestamInicio) {
		this.timestamInicio = timestamInicio;
	}

	public long getTimestamFin() {
		return timestamFin;
	}

	public void setTimestamFin(long timestamFin) {
		this.timestamFin = timestamFin;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	} 
	
	///////////////////////////////////////////////
	
	
	
	
	
}
