
public class Usuario extends Thread {

	private Servidor servidor; 
	private String nombre; 
	
	 
	
	public Usuario(String nombre ,Servidor servidor ) {
		super();
		this.servidor = servidor;
		this.nombre = nombre;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	//////////////////////////////////////////////////
	public void run() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 5; i++) {
			Usuario u = new Usuario (this.getNombre(), this.getServidor());
			this.getServidor().recepcionarPaquete(new PaqueteDato(u));
		}
		System.out.println("USUARIO: " + this.getNombre() + " termina de enviar paquetes");
	}





	

}
