import java.util.ArrayList;
import java.util.HashMap;

public class Administrador implements Runnable {

	private Servidor servidor;
	private HashMap<Usuario, ArrayList<PaqueteDato>> paquetes = new HashMap<Usuario, ArrayList<PaqueteDato>>();
	

	public Administrador(Servidor servidor) {
		super();
		this.servidor = servidor;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}


	public HashMap<Usuario, ArrayList<PaqueteDato>> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(HashMap<Usuario, ArrayList<PaqueteDato>> paquetes) {
		this.paquetes = paquetes;
	}
	
	////////////////////////////////////////////////////
	
	
	public void generaInforme() {
		
	}
	
	
	@Override
	public void run() {
		
		
	}

	
















}
