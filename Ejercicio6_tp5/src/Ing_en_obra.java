import java.util.Random;

public class Ing_en_obra extends Ingeniero_civil implements agrimensor{
	
	private Random random ; 
	
	public Ing_en_obra(String nombre, Servidor servidor) {
		super(nombre, servidor);
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < 5; i++) {
			try {
				this.enviarMedicion(this.medir());
			} catch (Falla_internet e) {
			}
		}
		this.getServidor().agregarDato(null);

	}

	@Override
	public Paquete_datos medir() {
		return new Paquete_datos((new Random().nextDouble()*90), (new Random().nextDouble()*180));
		
		//return new Paquete_datos(this.random.nextDouble()*90, this.random.nextDouble());
		
	}

	@Override
	public void enviarMedicion(Paquete_datos dato) throws Falla_internet {
		if (new Random().nextInt() < 40) {
			throw new Falla_internet(this.getNombre()); 
		}
		this.getServidor().agregarDato(dato);
	}

	public Random getRandom() {
		return random;
	}

	public void setRandom(Random random) {
		this.random = random;
	}
}
