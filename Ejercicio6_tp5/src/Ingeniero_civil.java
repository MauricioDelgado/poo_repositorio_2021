import java.util.ArrayList;

public class Ingeniero_civil extends Thread{
	
	private String nombre;
	private Servidor servidor;
	private ArrayList<Paquete_datos> col_paquete= new ArrayList<Paquete_datos>();
	
	public Ingeniero_civil(String nombre, Servidor servidor) {
		super();
		this.nombre = nombre;
		this.servidor = servidor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public ArrayList<Paquete_datos> getCol_paquete() {
		return col_paquete;
	}

	public void setCol_paquete(ArrayList<Paquete_datos> col_paquete) {
		this.col_paquete = col_paquete;
	} 
	
	public void informe() {
		Integer total = 0; 
		Integer aptos = 0; 
		
		for (Paquete_datos paquete_datos : col_paquete) {
			if(paquete_datos.getAnguloVertical() < 30) {
				aptos++;
			}
			total++;
		}
		System.out.println("De los "+ total +" terrenos medidos, " +aptos + " son aptos para construir");
	}
	
	public void run() {
		Integer contador = 0; 
		while(contador < 2) {
			Paquete_datos paquete = this.getServidor().obtenerDato(); 
			if (paquete == null) {
				contador++;
			}else {
				this.getCol_paquete().add(paquete);
			}
		}
		System.out.println("\nINFORME:");
	
		this.informe();
	
	
}
}