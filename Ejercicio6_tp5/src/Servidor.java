import java.util.LinkedList;

public class Servidor {
	private LinkedList<Paquete_datos> buffer  = new LinkedList<Paquete_datos>();
	

	public synchronized Paquete_datos obtenerDato() {
		while (buffer.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		Paquete_datos unPaquete = buffer.removeFirst();
		if (unPaquete != null) {
			System.out.println("OBTENGO DATO: " + unPaquete.toString());
		}
		notifyAll();
		return unPaquete;
		
	}
	
	public synchronized void agregarDato(Paquete_datos unPaqueteDatos) {
		while (this.buffer.size() >= 3) {
			 try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		
		if (unPaqueteDatos != null) {
			System.out.println("AGREGA DATO: " + unPaqueteDatos.toString());
		}
		buffer.add(unPaqueteDatos);
		notifyAll();
	}

	public LinkedList<Paquete_datos> getBuffer() {
		return buffer;
	}

	public void setBuffer(LinkedList<Paquete_datos> buffer) {
		this.buffer = buffer;
	}
	
	
}
