
public class Paquete_datos {
	private Double anguloVertical;
	private Double anguloHorizontal;
	
	public Paquete_datos(Double anguloVertical, Double anguloHorizontal) {
		super();
		this.anguloVertical = anguloVertical;
		this.anguloHorizontal = anguloHorizontal;
	}

	public Double getAnguloVertical() {
		return anguloVertical;
	}

	public void setAnguloVertical(Double anguloVertical) {
		this.anguloVertical = anguloVertical;
	}

	public Double getAnguloHorizontal() {
		return anguloHorizontal;
	}

	public void setAnguloHorizontal(Double anguloHorizontal) {
		this.anguloHorizontal = anguloHorizontal;
	}

	@Override
	public String toString() {
		return "Paquete_datos [anguloVertical=" + anguloVertical + ", anguloHorizontal=" + anguloHorizontal + "]";
	} 
	
	
	
	
	
	
}
