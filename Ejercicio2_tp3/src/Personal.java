
public abstract class Personal {
	private String nombre, sector;
	private Integer antiguedad; 
	
	public Personal() {
		this("", "",0); 
	}

	public Personal(String nombre, String sector, Integer antiguedad) {
		this.setNombre(nombre);
		this.setSector(sector);
		this.setAntiguedad(antiguedad);
	}

	protected String getNombre() {
		return nombre;
	}

	protected void setNombre(String nombre) {
		this.nombre = nombre;
	}

	protected String getSector() {
		return sector;
	}

	protected void setSector(String sector) {
		this.sector = sector;
	}

	protected Integer getAntiguedad() {
		return antiguedad;
	}

	protected void setAntiguedad(Integer antiguedad) {
		this.antiguedad = antiguedad;
	}
	
	// metodo abstracto
	public abstract Double calcular_horario();
	
}
