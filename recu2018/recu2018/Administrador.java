package recu2018;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Administrador implements Runnable {
	private Servidor servidor;
	private HashMap<Usuario, ArrayList<PaqueteDato>> paquetes = new HashMap<Usuario, ArrayList<PaqueteDato>>();

	public Administrador(Servidor servidor) {
		super();
		this.servidor = servidor;
	}

	@Override
	public void run() {
		int contador = 0;
		while (contador < 2) {
			try {
				PaqueteDato p = this.getServidor().enviarPaquete();
				if (p != null) {
					if (!this.getPaquetes().containsKey(p.getUsuario())) {
						ArrayList<PaqueteDato> paquetes = new ArrayList<PaqueteDato>();
						paquetes.add(p);
						this.getPaquetes().put(p.getUsuario(), paquetes);
					} else {
						this.getPaquetes().get(p.getUsuario()).add(p);
					}
				} else {
					contador++;
				}
			} catch (FallaTransferencia e) {
				System.err.println(e.getMessage());
			}
		}
		this.generarInforme();
	}

	public void generarInforme() {
		Set<Usuario> claves = this.getPaquetes().keySet();
		long promedio_1=0;
		System.out.println("\nINFORME:");
			for (Usuario c: claves) {
				for (PaqueteDato p : this.getPaquetes().get(c)) {
					promedio_1 += p.getTimestampFin();
				}
				promedio_1 /= this.getPaquetes().get(c).size();
				System.out.println(c.getNombre()+":\n"+"Promedio de tiempo en que tardaron sus paquetes: "+promedio_1+ " milisegundos.\n");
			}
			}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public HashMap<Usuario, ArrayList<PaqueteDato>> getPaquetes() {
		return paquetes;
	}

	public void setPaquetes(HashMap<Usuario, ArrayList<PaqueteDato>> paquetes) {
		this.paquetes = paquetes;
	}
}
