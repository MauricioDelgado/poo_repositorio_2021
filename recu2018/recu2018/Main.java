package recu2018;

public class Main {

	public static void main(String[] args) {
		Servidor servidor = new Servidor();
		Usuario u_1 = new Usuario("usuario 1", servidor);
		Usuario u_2 = new Usuario("usuario 2", servidor);
		Administrador admin = new Administrador(servidor);
		Thread hilo = new Thread(admin);
		
		u_1.start();
		u_2.start();
		hilo.start();

	}

}
