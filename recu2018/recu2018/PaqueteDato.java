package recu2018;

public class PaqueteDato {
private long timestampInicio = 0;
private long timestampFin = 0;
private Usuario usuario;
public PaqueteDato(Usuario usuario) {
	super();
	this.usuario = usuario;
}
public long getTimestampInicio() {
	return timestampInicio;
}
public void setTimestampInicio(long timestampInicio) {
	this.timestampInicio = timestampInicio;
}
public long getTimestampFin() {
	return timestampFin;
}
public void setTimestampFin(long timestampFin) {
	this.timestampFin = timestampFin;
}
public Usuario getUsuario() {
	return usuario;
}
public void setUsuario(Usuario usuario) {
	this.usuario = usuario;
}

}
