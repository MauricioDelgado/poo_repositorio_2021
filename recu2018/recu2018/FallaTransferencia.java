package recu2018;

public class FallaTransferencia extends Exception {

	private static final long serialVersionUID = 1L;

	public FallaTransferencia(String message) {
		super(message);
	}

}
