package recu2018;

import java.security.SecureRandom;

public class Usuario extends Thread {
private String nombre;
private Servidor servidor;
public Usuario(String nombre, Servidor servidor) {
	super();
	this.nombre = nombre;
	this.servidor = servidor;
}
public void run() {
	SecureRandom rdm = new SecureRandom();
for (int i = 0; i < 5; i++) {
	PaqueteDato p = new PaqueteDato(this);
	p.setTimestampFin(rdm.nextInt(1000));
	this.getServidor().recepcionarPaquete(p);
}	
this.getServidor().recepcionarPaquete(null);
System.out.println("USUARIO: "+ this.getNombre()+"termina de enviar paquetes");
}

public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public Servidor getServidor() {
	return servidor;
}
public void setServidor(Servidor servidor) {
	this.servidor = servidor;
}

}
