package recu2018;

import java.util.LinkedList;
import java.util.concurrent.ThreadLocalRandom;

import parcial_2018.FallaTrasladoException;

public class Servidor {
	private LinkedList<PaqueteDato>paquetes = new LinkedList<PaqueteDato>();
	
			
	public synchronized void recepcionarPaquete(PaqueteDato paquete) {
		while (this.getPaquetes().size() >= 2) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (paquete != null) {
			System.out.println("USUARIO:"+paquete.getUsuario().getNombre()+" entreg� paquete al servidor." );
		}
		this.getPaquetes().addLast(paquete);
		notifyAll();
	}
	public synchronized PaqueteDato enviarPaquete() throws FallaTransferencia {
		while (this.getPaquetes().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		PaqueteDato p = this.getPaquetes().removeFirst();
		if (ThreadLocalRandom.current().nextDouble() < 0.2) {
			throw new FallaTransferencia("Fallo en el env�o del paquete"); 
		}else {
			if (p != null) {
				System.out.println("ADMIN: Consume paquete de "+p.getUsuario().getNombre()+".");
			}
		}
		notifyAll();
		return p;
	}

	public LinkedList<PaqueteDato> getPaquetes() {
		return paquetes;
	}


	public void setPaquetes(LinkedList<PaqueteDato> paquetes) {
		this.paquetes = paquetes;
	}
}
