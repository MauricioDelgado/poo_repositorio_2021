import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		String cadena1, cadena2;
		char rta; 
		
		System.out.println("QUIERE INICIAR EL PROGRAMA? S/N ");
		rta = entrada.next().charAt(0);
		
		while ( rta == 'S') {
		
		
			System.out.println("Ingrese la primer palabra: ");
			cadena1 = entrada.nextLine();
		
			System.out.println("Ingrese la segunda palabra: ");
			cadena2 = entrada.nextLine();
			
			System.out.println("Con metodo == las cadenas ");
			if (cadena1 == cadena2) {
				System.out.println("Son iguales");
			}else {
				System.out.println("No son iguales");
			}
	
			System.out.println("Con metodo .equals las cadenas ");
			if (cadena1.equalsIgnoreCase(cadena2)) {
				System.out.println("Son iguales");
			}else {
				System.out.println("No son iguales");
			}
		
			System.out.println("QUIERE RE-INICIAR EL PROGRAMA? S/N ");
			rta = entrada.next().charAt(0);
		}
	}

}
