import java.time.LocalDate;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		Persona persona1 = new Persona("Mauricio", "M", "mauri@unEmail.com",
				LocalDate.of(1993, 04, 11), "z3pp3");
		Persona persona2 = new Persona("Pepe", "M", "pepe@unEmail.com",
				LocalDate.of(1993, 04, 11), "pepe");
		Persona persona3 = new Persona("Jolie", "F", "jolie@unEmail.com",
				LocalDate.of(1993, 04, 11), "jolie");
		
		Banco banco = new Banco(); 
		
		if(banco.agregarCuenta(persona1, 35.00)) {
			System.out.println("Persona agregada con exito");
		}else {
			System.out.println("No se pudo agreagar a la persona");
		}
		
		//veo el saldo de la persona1
		System.out.println(banco.getSaldo(persona1));
		
		// al querer agregar a la misma persona no puedo
		if(banco.agregarCuenta(persona1, 500.00)) {
			System.out.println("Persona agregada con exito");
		}else {
			System.out.println("No se pudo agreagar a la persona");
		}
		
		//agrego persona 2
		if(banco.agregarCuenta(persona2, 505.00)) {
			System.out.println("Persona agregada con exito");
		}else {
			System.out.println("No se pudo agreagar a la persona");
		}
		
		//veo saldo de persona 2
		if (banco.getSaldo(persona2) > 0.0) {
			System.out.println(banco.getSaldo(persona2));
		}else {
			System.out.println("La cuenta no existe");
		}
		
		// Elimino la persona 1
		if(banco.eliminarCuenta(persona1)) {
			System.out.println("Persona eliminada del sistema");
		}else {
			System.out.println("La persona no esta en el sistema");
		}
		
		if (banco.getSaldo(persona1) > 0.0) {
			System.out.println(banco.getSaldo(persona2));
		}else {
			System.out.println("La cuenta no existe");
		}
		
		// vuelvo a agregar a los 2 que saque 
		if(banco.agregarCuenta(persona1, 1000.00)) {
			System.out.println("Persona agregada con exito");
		}else {
			System.out.println("No se pudo agreagar a la persona");
		}
		
		if(banco.agregarCuenta(persona3, 1505.00)) {
			System.out.println("Persona agregada con exito");
		}else {
			System.out.println("No se pudo agreagar a la persona");
		}
		
		//  Se imprimen los clientes con su saldo
		banco.listarClientes();
		
		// Se imprime todos los datos del cliente
		//banco.listaCompleta();
	}

}
