import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Banco {
// ATRIBUTOS
	private String nombre; 
	private HashMap<Persona, Double> cuentas;
	

	public Banco() {
		this("", null); 
	}
	
	
// CONSTRUCTORES
	public Banco(String nombre, HashMap<Persona, Double> cuentas) {
		this.setNombre(nombre);
		//this.setCuentas(cuentas);
		this.cuentas = new HashMap<>();
	}

// METODOS
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public HashMap<Persona, Double> getCuenta() {
		return cuentas;
	}

	public void setCuentas(HashMap<Persona, Double> cuentas) {
		this.cuentas = cuentas;
	}
	
// METODOS PROPIOS	
	public Boolean agregarCuenta(Persona persona, Double saldo) {
		if(!cuentas.containsKey(persona)) {
			cuentas.put(persona, saldo);
			return true;
		}else {
			return false;
		}
	}
	
	public Boolean eliminarCuenta(Persona persona) {
		if(cuentas.containsKey(persona)) {
			cuentas.remove(persona);
			return true;
		}else {
			return false;
		}
		
	}
	
	public void listarClientes() {
		Iterator cliente = cuentas.keySet().iterator();
		while (cliente.hasNext()) {
			Persona nombre = (Persona) cliente.next();
			System.out.println("Nombre Cliente: " + nombre.getNombre()+ ": saldo= " + cuentas.get(nombre));
		}
	}
	
	public void listaCompleta( ) {
		System.out.println(cuentas); //-->con esto imprime todo, datos de la persona y saldo

	}
	
	public Double getSaldo(Persona persona) {
		for(Map.Entry<Persona, Double> entrada : cuentas.entrySet()) {
			if(entrada.getKey().equals(persona))
			{
				return entrada.getValue();
			}
		}
			return -1.0; 
	}
	
	
}
