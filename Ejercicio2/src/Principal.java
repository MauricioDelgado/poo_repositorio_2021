import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		
		String cadena1, cadena2;
		System.out.println("Ingrese la primer frase: ");
		cadena1 = entrada.nextLine();
		
		System.out.println("Ingrese la segunda frase: ");
		cadena2 = entrada.nextLine();
		
		System.out.println("El resultado de las dos cadenas ingresadas son: " + cadena1 +" "+ cadena2);
	}

}
