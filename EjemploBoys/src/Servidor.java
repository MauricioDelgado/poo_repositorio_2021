import java.util.LinkedList;

public class Servidor {

	private LinkedList<PaqueteDato> colaPaquetes = new LinkedList<PaqueteDato>();
	
	public synchronized void almacenar(PaqueteDato unPaquete) {
		
		while(colaPaquetes.size() >= 3) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(unPaquete != null) {
			System.out.println("ALAMCENO: "+unPaquete.toString());
		}
		colaPaquetes.add(unPaquete);
		notifyAll();
	}
	
	public synchronized PaqueteDato consultar() {
		while(colaPaquetes.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		PaqueteDato unPaquete = colaPaquetes.removeFirst();
		if(unPaquete != null) {
			System.out.println("CONSULTO: "+unPaquete.toString());
		}
		notifyAll();
		return unPaquete;
	}
	
}
