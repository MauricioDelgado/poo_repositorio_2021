package ejemploconexionbd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BasedeDatos {
   private Connection conexion;
   private String url= "jdbc:postgresql://localhost/Usuarios";
   private String usuario="postgres";
   private String clave="basededatos2021";
   private static BasedeDatos bd = null;
   
   private BasedeDatos() {
	   try {
			DriverManager.registerDriver(new org.postgresql.Driver());
			this.setConexion(DriverManager.getConnection(url,usuario,clave));
		} catch (SQLException e) {
			e.printStackTrace();
   }
	
}

public Connection getConexion() {
	return conexion;
}

private void setConexion(Connection conexion) {
	this.conexion = conexion;
}
   public static BasedeDatos getInstance() {
	   return (bd == null)? new BasedeDatos() : bd;
   }
   
   public ResultSet getAll(String consulta) {
	   ResultSet rs = null;
	   
	   try {
	    Statement s = conexion.createStatement();
		rs = s.executeQuery(consulta);
	} catch (SQLException e) {
		e.printStackTrace();
	}
	   return rs;
   }
   
   public Integer alta(String tabla, String columnas, String valores) { //ALTA
	   /*INSERT INTO public.usuario (
			   nombre, contrasenia) VALUES (
			   'nabil','0000')
			    returning id;*/
	   try {
		PreparedStatement ps = conexion.prepareStatement("INSERT INTO public." + tabla + " ("+columnas+" ) VALUES ("+valores+") returning id;");
		
		if (ps.execute()) {
			ResultSet rs = ps.getResultSet();
			rs.next();
			return rs.getInt(1);
		}else {
			Integer entero = ps.getUpdateCount();
			System.out.println("Devuelve por getUpdateCount "+entero+".");
		}
		return -1;
	} catch (SQLException e) {
		e.printStackTrace();
	} return -1;
	   
   }
    public Boolean update(String tabla, Integer id, String valores) { //MODIFICACION
    	/*UPDATE public.usuario SET
    	nombre = 'nabil1', contrasenia = '00000' WHERE
    	id = 1;*/
    	try {
    		PreparedStatement ps = conexion.prepareStatement("UPDATE public." + tabla + " SET "+valores+" WHERE id="+id+";");
    		
    		ps.execute();
    		Integer entero = ps.getUpdateCount();
    		return entero>0;
    	} catch (SQLException e) {
    		e.printStackTrace();
    	} return false;
    }
    public Boolean remove(String tabla, Integer id) { //BAJA
    
    	Statement st;
		try {
			st = conexion.createStatement();
			Integer entero = st.executeUpdate("DELETE FROM public."+tabla+" WHERE id IN ("+id+");");
			return entero > 0;
		} catch (SQLException e) {
			
			e.printStackTrace();
		}return false;

    }
}
