package ejemploconexionbd;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Main {

	public static void main(String[] args) {
		UsuarioDao usuarioDao = new UsuarioDao();
		

		usuarioDao.insert(new Usuario("Tomas", "1111"));
		for (Usuario u1 : usuarioDao.getAll()) {
			System.out.println(u1);
		}
		Usuario u = usuarioDao.get(1);
		u.setContrasenia("1234");
		u.setNombre("Matias");
		usuarioDao.update(u);
		System.out.println("");
		for (Usuario u2 : usuarioDao.getAll()) {
			System.out.println(u2);
		}
		System.out.println("Borrando elementos:");
		usuarioDao.remove(u);
		for (Usuario u3 : usuarioDao.getAll()) {
			System.out.println(u3);
		}
		
	}
}
