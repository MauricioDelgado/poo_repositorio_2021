package ejemploconexionbd;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioDao {
	
	
	public ArrayList<Usuario> getAll(){
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT * FROM public.usuario");
		try {
			while (rs.next()) {
				Integer id = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String contrasenia = rs.getString("contrasenia");
				usuarios.add(new Usuario(id,nombre,contrasenia));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usuarios;
	}
	public Usuario get(Integer id) {
		Usuario usuario = null;
		ResultSet rs = BasedeDatos.getInstance().getAll("SELECT * FROM public.usuario where id="+ id);
		try {
			while (rs.next()) {
				Integer idrs = rs.getInt("id");
				String nombre = rs.getString("nombre");
				String contrasenia = rs.getString("contrasenia");
				usuario = new Usuario(idrs,nombre,contrasenia);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return usuario;
		
	}
	public Integer insert(Usuario u) {
		return BasedeDatos.getInstance().alta("usuario","nombre,contrasenia" ,"'"+u.getNombre()+"','"+u.getContrasenia()+"'");
	}
	public Boolean update(Usuario u) {
		return BasedeDatos.getInstance().update("usuario", u.getId(), "nombre = '"+u.getNombre()+"', contrasenia = '"+u.getContrasenia()+"'");
	}
	public Boolean remove(Usuario u){
		return BasedeDatos.getInstance().remove("usuario",u.getId());
	}
}
