import java.util.LinkedList;

public class Servidor {

	private LinkedList<Jugador> jugadores = new LinkedList<Jugador>();

	public LinkedList<Jugador> getJugadores() {
		return jugadores;
	}

	public void setJugadores(LinkedList<Jugador> juagadores) {
		this.jugadores = juagadores;
	}
	
	
	
	//////////////////////////////////7

	public synchronized void Sala(Jugador jugador) {
		while (this.getJugadores().size() >= 4) {
			 try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		
		if (jugador != null) {
			
			System.out.println("INGRESO: " + jugador.getUsuario() + " ha ingresado en la sala");
			
		}
		this.getJugadores().add(jugador);
		notifyAll();
	}



	public synchronized Jugador consulta() {
		while (this.getJugadores().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		Jugador unJugador = this.getJugadores().removeFirst();
	
		notifyAll();
		return unJugador;

		
	}



}
