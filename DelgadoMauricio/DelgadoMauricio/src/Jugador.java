import java.util.Random;

public class Jugador {
 private String usuario;
 private Integer nivel = new Random().nextInt(99)+1;
 private String pais;

 public Jugador(String usuario, String pais) {
	super();
	this.usuario = usuario;
	this.pais = pais;
}

public String getUsuario() {
	return usuario;
}

public void setUsuario(String usuario) {
	this.usuario = usuario;
}

public Integer getNivel() {
	return nivel;
}

public void setNivel(Integer nivel) {
	this.nivel = nivel;
}

public String getPais() {
	return pais;
}

public void setPais(String pais) {
	this.pais = pais;
}
 //////////////////////////////////////
public Integer precisionTiro() {
	
	return this.getNivel()* new Random().nextInt(5)+1;
	
}

@Override
public String toString() {
	return "Jugador [usuario=" + usuario + ", nivel=" + nivel + ", pais=" + pais + "]";
}
 

 
 
 
}
