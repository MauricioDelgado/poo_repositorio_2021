import java.util.ArrayList;
import java.util.LinkedList;

public class Juego extends Thread{

	private LinkedList<Jugador> equipo = new LinkedList<Jugador>();
	private Servidor servidor;
	
	
	public Juego(Servidor servidor) {
		super();
		this.servidor = servidor;
	}

	public LinkedList<Jugador> getEquipo() {
		return equipo;
	}

	public void setEquipo(LinkedList<Jugador> equipo) {
		this.equipo = equipo;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	////////////////////////////////////////////////////////
	public void run () {
		int contador =0; 
		
		while(contador < 2) {
			
				Jugador jug = this.getServidor().consulta();
				if (jug == null) {
					contador++; 
				} else {
					this.getEquipo().add(jug);
				}
		}
		this.armarPartida();
		System.out.println("SE TERMINA PARTIDA");
	}
	
	
	
	public void armarPartida() {
		//LinkedList<Jugador> restante = new LinkedList<Jugador>();
	
	
		while (this.getEquipo().size() > 1) {
			Jugador jug1 = this.getEquipo().removeFirst(); 
			Jugador jug2 = this.getEquipo().removeFirst();
			//restante.add(this.enfrentarse(jug1,jug2));
			this.getEquipo().add(this.enfrentarse(jug1,jug2));
		}
	
	
		System.out.println("EL GANADOR DE LA PARTIDA ES: " + this.getEquipo().getFirst().toString());
	}
	
	
	public Jugador enfrentarse (Jugador jug1,Jugador jug2) {
		
		System.out.println("ENFRENTAMIENTO: "+ jug1.getUsuario() + " y " + jug2.getUsuario()) ;
		
		if (jug1.precisionTiro() > jug2.precisionTiro()) {
			System.out.println("GANADOR: " + jug1.getUsuario());
			return jug1;
		}else {
			System.out.println("GANADOR: " + jug2.getUsuario());
			return jug2;
		}
	}
	
}
