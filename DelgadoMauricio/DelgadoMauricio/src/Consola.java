import java.util.Random;

public class Consola extends Thread implements Conexion{

	private Servidor sevidor;

	
	
	public Consola(Servidor sevidor) {
		super();
		this.sevidor = sevidor;
	}

	public Servidor getSevidor() {
		return sevidor;
	}

	public void setSevidor(Servidor sevidor) {
		this.sevidor = sevidor;
	}
	
	///////////////////////////////////////7
	
	public void realizarConexion(String nombre) throws FallaConexion {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(new Random().nextInt(100) < 35) {
			throw new FallaConexion("FALLO EL USUARIO "+ nombre +" INTENTO CONECTARSE SIN EXITO DESDE CONSOLA");
		}
		String pais = "";
		
		Integer numero = new Random().nextInt(100);
		
		if(numero < 50 ) {
			pais = "Argentina";
		} else  {
			pais = "Chile";
		} 
		
		this.getSevidor().Sala(new JugadorConsola(nombre, pais));
	}
	
	public  void run() {

		for (int i = 0; i < 10; i++) {
			try {
				String jugador = "jugadorConsola" + i;
			this.realizarConexion(jugador);
			} catch (FallaConexion e) {
				//e.printStackTrace();
			}
		}
		this.getSevidor().Sala(null);
	}
	
}
