import java.util.Random;


public class Pc extends Thread implements Conexion{

	private Servidor sevidor;

	
	
	public Pc(Servidor sevidor) {
		super();
		this.sevidor = sevidor;
	}

	public Servidor getSevidor() {
		return sevidor;
	}

	public void setSevidor(Servidor sevidor) {
		this.sevidor = sevidor;
	}
	
	///////////////////////////////////////7
	@Override
	public void realizarConexion(String nombre) throws FallaConexion {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if(new Random().nextInt(100) < 20) {
			throw new FallaConexion("FALLO EL USUARIO "+ nombre +" INTENTO CONECTARSE SIN EXITO DESDE PC");
		}
		String pais ="";
		
		Integer numero = new Random().nextInt(100);
		
		if(numero < 33 ) {
			pais = "Paraguay";
		} else if (numero > 33 && numero < 66 ) {
			pais = "Peru";
		} else if (numero > 66 && numero < 100 ) {
			pais = "Brasil";
		} 
		
		this.getSevidor().Sala(new JugadorPc(nombre, pais));
	}
	
	public  void run() {
	
		for (int i = 0; i < 10; i++) {
			try {
				String jugador = "jugadorPC" + i;
				this.realizarConexion(jugador);
			} catch (FallaConexion e) {
				//e.printStackTrace();
			}
		}
		
		this.getSevidor().Sala(null);
	}
}
