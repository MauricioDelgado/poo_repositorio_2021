import java.security.SecureRandom;
import java.util.Scanner;


public class Principal {

	public static void main(String[] args) {
		Scanner entrada = new Scanner (System.in);
		
		int numero, aleatorio; 
		char rta; 
		
		System.out.println("QUIERE INICIAR EL PROGRAMA? S/N ");
		rta = entrada.next().charAt(0);
		
		while ( rta == 'S') {
			
			System.out.println("Ingrese un numero del 1 al 10: ");
			numero = entrada.nextInt();
			
			SecureRandom sran = new SecureRandom();
			aleatorio = sran.nextInt(11);
			
		
			System.out.println(numero == aleatorio? (numero + " es igual a ") + aleatorio : (numero + " es distinto a ") + aleatorio );
			System.out.println(numero > aleatorio? (numero + " es mayor a ") + aleatorio : (numero + " es menor a ") + aleatorio );
			System.out.println(numero >= aleatorio? (numero + " es mayor o igual a ") + aleatorio : (numero + " es menor o igual a ") + aleatorio );

			System.out.println("QUIERE RE-INICIAR EL PROGRAMA? S/N ");
			rta = entrada.next().charAt(0);
		}

		
		
	
	}

}
