package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import modelo.Usuario;
import vista.Vista_login;

public class Controlador_login implements  ActionListener {

	private Vista_login vl; 
	private Usuario usuario;
	

	public Controlador_login() {
		super();
		this.setUsuario(new Usuario());
		this.setVl(new Vista_login(this));
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
	
	JButton btn = (JButton) e.getSource();
	Usuario usuario = new Usuario("Mauricio", "pepe123", 1, 1);
	
	
	if (btn.getText().equals("INGRESAR")) {
		String user = getVl().getTxtUsuario().getText(); 
		String pass = getVl().getTxtContrasenia().getText();
			if(usuario.getNombre_usuario().equals(user) && usuario.getContrasenia().equals(pass)) {
				JOptionPane.showMessageDialog(null, "Ingreso exitoso");
				}else {
				JOptionPane.showMessageDialog(null, "Ingreso Incorrecto");
				}
		}
		
	}
		
	
	public Vista_login getVl() {
		return vl;
	}
	public void setVl(Vista_login vl) {
		this.vl = vl;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	

	
	
}
