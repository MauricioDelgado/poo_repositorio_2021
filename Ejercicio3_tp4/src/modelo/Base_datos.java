package modelo;

import java.util.ArrayList;
import java.util.Iterator;

public class Base_datos {

	private ArrayList<Usuario> usuarios;

	public Base_datos() {
	
	}
	
	public Base_datos(ArrayList<Usuario> usuarios) {
		super();
		this.setUsuarios(usuarios);
	}
	
	public ArrayList<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(ArrayList<Usuario> usuarios) {
		this.usuarios = usuarios;
	}


	public void agregar_usuario(Usuario	usuario) {
		if (validadacion(usuario)) {
			System.out.println("La persona ya esta");
		}else {
			usuarios.add(usuario);
			System.out.println("persona agregada");
		}
		
		
	}

	private boolean validadacion(Usuario usuario2) {
		Iterator<Usuario> persona = usuarios.iterator();
		Boolean encontrado = false;
		
		while (persona.hasNext() && !encontrado == true) {
			Usuario per = persona.next();
			if (per.getId_usuario() == usuario2.getId_usuario()) {
				encontrado = true; 
			}else {
				encontrado=  false; 
			}
		}
		if (encontrado) {
			return true; 
		} else {
			return false; 
		}
	}
	
}
