package modelo;

public class Usuario {

	private String nombre_usuario,contrasenia;  
	private Integer tipo_usuario, id_usuario;
	

	public Usuario() {
		this("", "", -1, -1);
	}

	public Usuario(String nombre_usuario, String contrasenia, Integer tipo_usuario, Integer id_usuario) {
		super();
		this.setNombre_usuario(nombre_usuario);
		this.setContrasenia(contrasenia);
		this.setTipo_usuario(tipo_usuario);
		this.setId_usuario(id_usuario);
	}

	public String getNombre_usuario() {
		return nombre_usuario;
	}

	public void setNombre_usuario(String nombre_usuario) {
		this.nombre_usuario = nombre_usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public Integer getTipo_usuario() {
		return tipo_usuario;
	}

	public void setTipo_usuario(Integer tipo_usuario) {
		this.tipo_usuario = tipo_usuario;
	}

	public Integer getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(Integer id_usuario) {
		this.id_usuario = id_usuario;
	}
	
	
}
