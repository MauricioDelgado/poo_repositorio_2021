package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.Controlador_login;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Vista_login extends JFrame {

	private JPanel contentPane;
	private JLabel lblUsuario;
	private JLabel lblContrasenia;
	private JTextField txtUsuario;
	private JTextField txtContrasenia;
	private JButton btnIngresar;
	
	private Controlador_login cl; 
	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public Vista_login(Controlador_login cl) {
		setCl(cl);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 434, 260);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblUsuario = new JLabel("USUARIO: ");
		lblUsuario.setBounds(31, 82, 61, 20);
	
		contentPane.add(lblUsuario);
		
		
		lblContrasenia = new JLabel("CONTRASE\u00D1A:");
		lblContrasenia.setBounds(31, 131, 85, 20);
		contentPane.add(lblContrasenia);
		
		JLabel lblTitulo = new JLabel("\u00A1BIENVENIDO!");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTitulo.setBounds(130, 11, 169, 39);
		contentPane.add(lblTitulo);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(146, 82, 234, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtContrasenia = new JTextField();
		txtContrasenia.setColumns(10);
		txtContrasenia.setBounds(146, 131, 234, 20);
		contentPane.add(txtContrasenia);
		
		btnIngresar = new JButton("INGRESAR");
		btnIngresar.addActionListener(getCl()); // actionlistener
		btnIngresar.setBounds(10, 176, 398, 23);
		contentPane.add(btnIngresar);
	}


	public JLabel getLblUsuario() {
		return lblUsuario;
	}


	public void setLblUsuario(JLabel lblUsuario) {
		this.lblUsuario = lblUsuario;
	}


	public JLabel getLblContrasenia() {
		return lblContrasenia;
	}


	public void setLblContrasenia(JLabel lblContrasenia) {
		this.lblContrasenia = lblContrasenia;
	}


	public JTextField getTxtUsuario() {
		return txtUsuario;
	}


	public void setTxtUsuario(JTextField txtUsuario) {
		this.txtUsuario = txtUsuario;
	}


	public JTextField getTxtContrasenia() {
		return txtContrasenia;
	}


	public void setTxtContrasenia(JTextField txtContrasenia) {
		this.txtContrasenia = txtContrasenia;
	}


	public JButton getBtnIngresar() {
		return btnIngresar;
	}


	public void setBtnIngresar(JButton btnIngresar) {
		this.btnIngresar = btnIngresar;
	}


	public Controlador_login getCl() {
		return cl;
	}


	public void setCl(Controlador_login cl) {
		this.cl = cl;
	}
	
	
}
