import java.util.ArrayList;
import java.util.Iterator;

public class MainCompaniaTelefonica {

	public static void main(String[] args) {
		
		Claro usuario1 = new Claro(0.45, 13.0, 29.0, 50, 120, 50,20); 
		Personal usuario2 = new Personal(0.45, 13.0, 29.0, 50, 120, 50, 20, 50);
		Movistar usuario3 = new Movistar(0.45, 13.0, 29.0, 50, 120, 50, 20, 30, 10); 
		
		
		/*String clase = usuario1.getClass().toString().substring(5);
		String clase2 = usuario2.getClass().toString().substring(5);
		String clase3 = usuario3.getClass().toString().substring(5);
		
		System.out.println("El usuario de "+ clase +" paga: ");
		System.out.format("%.2f",usuario1.calcularTarifa());
		
		System.out.println("\nEl usuario de "+ clase2 +" paga: ");
		System.out.format("%.2f",usuario2.calcularTarifa());
		
		System.out.println("\nEl usuario de "+ clase3 +" paga: ");
		System.out.format("%.2f",usuario3.calcularTarifa());

		*/
		
		ArrayList<CompaniaTelefonica> usuarios = new ArrayList<CompaniaTelefonica>(); 
		usuarios.add(usuario1);
		usuarios.add(usuario2);
		usuarios.add(usuario3);
		
		Iterator<CompaniaTelefonica> personas = usuarios.iterator(); 
		
		while (personas.hasNext()) {
			CompaniaTelefonica usuario= (CompaniaTelefonica) personas.next();
			System.out.print("\nEl usuario de "+usuario.getClass().toString().substring(6) +" paga: $ ");
			System.out.format("%.2f",usuario.calcularTarifa());
		}
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
