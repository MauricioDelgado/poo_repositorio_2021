
public class CompaniaTelefonica {

	private Double valorMensajeTexto;
	private Double valorMinutoLLamada;
	private Double valorPaqueteDatos;
	private Integer cantidadMensajes;
	private Integer cantidadMinutosLlamada;
	private Integer cantidadMinutosDatos;
	
	public CompaniaTelefonica() {
		this(0.0,0.0,0.0, 0, 0, 0);
	}
	
	public CompaniaTelefonica(Double valorMensajeTexto, Double valorMinutoLLamada, Double valorPaqueteDatos,
			Integer cantidadMensajes, Integer cantidadMinutosLlamada, Integer cantidadMinutosDatos) {
		super();
		this.setValorMensajeTexto(valorMensajeTexto);
		this.setValorMinutoLLamada(valorMinutoLLamada);
		this.setValorPaqueteDatos(valorPaqueteDatos);
		this.setCantidadMensajes(cantidadMensajes);
		this.setCantidadMinutosLlamada(cantidadMinutosLlamada);
		this.setCantidadMinutosDatos(cantidadMinutosDatos);
	}

	protected Double getValorMensajeTexto() {
		return valorMensajeTexto;
	}

	protected void setValorMensajeTexto(Double valorMensajeTexto) {
		this.valorMensajeTexto = valorMensajeTexto;
	}

	protected Double getValorMinutoLLamada() {
		return valorMinutoLLamada;
	}

	protected void setValorMinutoLLamada(Double valorMinutoLLamada) {
		this.valorMinutoLLamada = valorMinutoLLamada;
	}

	protected Double getValorPaqueteDatos() {
		return valorPaqueteDatos;
	}

	protected void setValorPaqueteDatos(Double valorPaqueteDatos) {
		this.valorPaqueteDatos = valorPaqueteDatos;
	}
	
	public Integer getCantidadMensajes() {
		return cantidadMensajes;
	}

	public void setCantidadMensajes(Integer cantidadMensajes) {
		this.cantidadMensajes = cantidadMensajes;
	}

	public Integer getCantidadMinutosLlamada() {
		return cantidadMinutosLlamada;
	}

	public void setCantidadMinutosLlamada(Integer cantidadMinutosLlamada) {
		this.cantidadMinutosLlamada = cantidadMinutosLlamada;
	}

	public Integer getCantidadMinutosDatos() {
		return cantidadMinutosDatos;
	}

	public void setCantidadMinutosDatos(Integer cantidadMinutosDatos) {
		this.cantidadMinutosDatos = cantidadMinutosDatos;
	}
		
	
	public Double calcularTarifa() {		
		return  (this.getValorMinutoLLamada() * Double.valueOf(this.getCantidadMinutosLlamada())) +
				(this.getValorMensajeTexto() * Double.valueOf(this.getCantidadMensajes()))+
				(this.getValorPaqueteDatos() * Double.valueOf(this.getCantidadMinutosDatos()));
	}


	
}
