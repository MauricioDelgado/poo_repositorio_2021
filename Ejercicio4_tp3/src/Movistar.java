
public class Movistar extends CompaniaTelefonica implements Conversor {

	private Integer extraLlamada; 
	private Integer extraDatos; 
	private Integer extraMensaje; 
	
	public Movistar() {
		this(0.0,0.0,0.0,0,0,0,0,0,0);
	}

	public Movistar(Double valorMensajeTexto, Double valorMinutoLLamada, Double valorPaqueteDatos,
			Integer cantidadMensajes, Integer cantidadMinutosLlamada, Integer cantidadMinutosDatos, Integer extraLlamada,Integer extraDatos ,Integer extraMensaje) {
		super(valorMensajeTexto, valorMinutoLLamada, valorPaqueteDatos, cantidadMensajes, cantidadMinutosLlamada,
				cantidadMinutosDatos);
		this.setExtraDatos(extraDatos);
		this.setExtraLlamada(extraLlamada);
		this.setExtraMensaje(extraMensaje);
	}

	public Integer getExtraLlamada() {
		return extraLlamada;
	}

	public void setExtraLlamada(Integer extraLlamada) {
		this.extraLlamada = extraLlamada;
	}

	public Integer getExtraDatos() {
		return extraDatos;
	}

	public void setExtraDatos(Integer extraDatos) {
		this.extraDatos = extraDatos;
	}

	public Integer getExtraMensaje() {
		return extraMensaje;
	}

	public void setExtraMensaje(Integer extraMensaje) {
		this.extraMensaje = extraMensaje;
	}

	
	@Override
	public Double conversor(Integer costoExtra) {
		return Double.valueOf(costoExtra)/ 100;
	}

	@Override
	public Double calcularTarifa() {
		Double totalLlamada = (this.getValorMinutoLLamada() * Double.valueOf(this.getCantidadMinutosLlamada())); 
		Double totalMensaje = (this.getValorMensajeTexto() * Double.valueOf(this.getCantidadMensajes())); 
		Double totalDatos= (this.getValorPaqueteDatos() * Double.valueOf(this.getCantidadMinutosDatos())); 
		
		return 
		(totalLlamada + (totalLlamada* conversor(this.getExtraLlamada()))) +
		(totalDatos + (totalDatos* conversor(this.getExtraDatos()))) +
		(totalMensaje + (totalMensaje* conversor(this.getExtraMensaje()))) ;
	}

	
}
