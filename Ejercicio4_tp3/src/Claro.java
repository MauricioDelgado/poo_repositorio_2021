
public class Claro extends CompaniaTelefonica implements Conversor {

	private Integer costoExtra; 
	
	public Claro() {
		this(0.0,0.0,0.0,0,0,0,0);
	}

	public Claro(Double valorMensajeTexto, Double valorMinutoLLamada, Double valorPaqueteDatos,
			Integer cantidadMensajes, Integer cantidadMinutosLlamada, Integer cantidadMinutosDatos, Integer costoExtra) {
		super(valorMensajeTexto, valorMinutoLLamada, valorPaqueteDatos, cantidadMensajes, cantidadMinutosLlamada,
				cantidadMinutosDatos);
		this.setCostoExtra(costoExtra);
	}

	public Integer getCostoExtra() {
		return costoExtra;
	}

	public void setCostoExtra(Integer costoExtra) {
		this.costoExtra = costoExtra;
	}

	public Double conversor(Integer costoExtra) {
		return Double.valueOf(costoExtra)/ 100;
	}
	
	@Override
	public Double calcularTarifa() {
		Double extra= super.calcularTarifa()* this.conversor(this.getCostoExtra()); 
		return super.calcularTarifa()+ extra;
	}

	
	
}
