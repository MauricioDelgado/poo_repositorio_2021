import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class LeerCSV {

	public static void main(String[] args) {
			try (Scanner entrada = new Scanner(Paths.get("assets/DatosPersonas.csv"))) {
				/*System.out.printf("%-10s%n %-12s%n %-12s%n ",
				*"Nombre:", "APELLIDO:", "DNI:");
				*/
				
				entrada.useDelimiter("\\r\\n|\\r\\n");
				
				String[] datos; 
			
				while(entrada.hasNext()) {
				datos = entrada.next().split(";");
				System.out.printf("%-10s%n %-10s%n %-10s%n ",
						"Nombre: "+ datos[0], "APELLIDO: " + datos[1], "DNI: "+ Integer.valueOf(datos[2]));
								
			}
		}catch (IOException e) {
			e.printStackTrace();
		}

		}

	}


//\\r   -->busque salto de linea y ponga puntero en siguiente salto de linea
//\\n  --> busca el salto y lo pasa abajo 
//el or | para quue lo haga al reves