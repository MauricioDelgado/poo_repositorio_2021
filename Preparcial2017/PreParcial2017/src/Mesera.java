import java.util.LinkedList;

public class Mesera {
	private LinkedList <Pedido> listaPedidos = new LinkedList<Pedido>();
	
	public synchronized void tomarOrden(Pedido pedido) {
		while (this.getListaPedidos().size() > 2) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(pedido != null) {
			System.out.println("TOMA PEDIDO: "+pedido.toString());
		}
		this.getListaPedidos().add(pedido);
		notifyAll();
	}
	
	public synchronized Pedido entregarACocina() {
		while (this.getListaPedidos().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Pedido unPedido = this.getListaPedidos().removeFirst();
		notifyAll();
		return unPedido;
	}

	public LinkedList<Pedido> getListaPedidos() {
		return listaPedidos;
	}

	public void setListaPedidos(LinkedList<Pedido> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}
	

}
