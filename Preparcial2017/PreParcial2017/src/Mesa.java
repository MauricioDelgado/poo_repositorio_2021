import java.util.Random;

public class Mesa implements Runnable, Atendible{
	private Integer nro;
	private Mesera mesera;

	public Mesa(Integer nro, Mesera mesera) {
		super();
		this.nro = nro;
		this.mesera = mesera;
	}

	@Override
	public Pedido generarPedido() {
		return new Pedido("Mesa nro: "+this.getNro(), "un pedido de comida");
	}

	@Override
	public void run() {
		int rand = new Random().nextInt(7) + 1;
		for (int i = 0; i < rand; i++) {
			this.getMesera().tomarOrden(this.generarPedido());
		}
		this.getMesera().tomarOrden(null);
	}

	public Integer getNro() {
		return nro;
	}

	public void setNro(Integer nro) {
		this.nro = nro;
	}

	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}
	
	

}
