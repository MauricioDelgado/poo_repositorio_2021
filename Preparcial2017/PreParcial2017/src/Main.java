
public class Main {

	public static void main(String[] args) {
		Mesera mesera = new Mesera();
		new Delivery (mesera).start();
		new Cocina (mesera).start();
		Thread t1 = new Thread(new Mesa(1, mesera));
		t1.setName("Mesa");
		t1.start();
	}

}
