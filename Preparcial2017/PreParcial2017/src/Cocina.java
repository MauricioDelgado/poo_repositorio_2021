import java.util.Random;

public class Cocina extends Thread {
	private Mesera mesera;

	public Cocina(Mesera mesera) {
		super();
		this.mesera = mesera;
	}

	private void prepararPedido(Pedido pedido) throws PerdidaDePedido {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if (new Random().nextInt(100) < 21) {
			throw new PerdidaDePedido();
		}
		System.out.println("PREPARANDO en cocina " + pedido.toString());
	}

	public void run() {
		int contador = 0;
		while (contador < 2) {
			Pedido pedidoCocina = this.getMesera().entregarACocina();
			if (pedidoCocina == null) {
				contador++;
			} else {
				try {
					this.prepararPedido(pedidoCocina);
				} catch (PerdidaDePedido e) {
					System.out.println("La cocina perdio el pedido de: " + pedidoCocina.getOrigen());
				}
			}
		}
		System.out.println("\nCierra la cocina.");
	}

	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}

}
