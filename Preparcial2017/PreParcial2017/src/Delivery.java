import java.util.Random;

public class Delivery extends Thread implements Atendible{
	private Mesera mesera;

	public Delivery(Mesera mesera) {
		super();
		this.mesera = mesera;
	}
	
	public void run() {
		int rand = new Random().nextInt(4) + 1;
		for (int i = 0; i < rand; i++) {
			this.getMesera().tomarOrden(this.generarPedido());
		}
		this.getMesera().tomarOrden(null);
	}

	public Mesera getMesera() {
		return mesera;
	}

	public void setMesera(Mesera mesera) {
		this.mesera = mesera;
	}

	@Override
	public Pedido generarPedido() {
		return new Pedido("Delivery", "un pedido de comida");
	}
	
	
	
}
