
public class Venta {

	private Integer numero; 
	private String fecha;
	
	public Venta(Integer numero, String fecha) {
		super();
		this.numero = numero;
		this.fecha = fecha;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Ticket:"
				+ "\nEl se�or de los anillos "
				+ "\nVenta [numero=" + numero + ", fecha=" + fecha + "]";
	} 
	
	
}
