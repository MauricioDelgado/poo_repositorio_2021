import java.util.LinkedList;

public class MaquinaFiscal {
  private LinkedList<Venta> ventas = new LinkedList<Venta>();

public MaquinaFiscal() {
	super();
	this.setVentas(ventas);
}

public LinkedList<Venta> getVentas() {
	return ventas;
}

public void setVentas(LinkedList<Venta> ventas) {
	this.ventas = ventas;
}
  
  
public synchronized void vender(Venta venta) {
    while (this.getVentas().size() >= 3) {
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}
	if (venta != null) {
		//if(venta.getClass().getTypeName().equalsIgnoreCase("Online")) {
		//}
		System.out.println(venta.toString());
	
	}
	this.getVentas().add(venta);
	notifyAll();
}
  
public synchronized Venta consultar() {
	while (this.getVentas().isEmpty()) {
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
	}
	
	notifyAll();
	return this.getVentas().removeFirst();
}
  


}
