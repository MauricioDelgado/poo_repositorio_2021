import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class PtoVenta  extends Thread{

	private MaquinaFiscal maquina;

	public PtoVenta(MaquinaFiscal maquina) {
		super();
		this.maquina = maquina;
	}

	public MaquinaFiscal getMaquina() {
		return maquina;
	}

	public void setMaquina(MaquinaFiscal maquina) {
		this.maquina = maquina;
	} 
	
	
	public void realizarVenta() {
		Integer boleto = (ThreadLocalRandom.current().nextInt(10000)+8999);
		Venta venta = new Venta(boleto, "24/10/2021 22:00");
		this.getMaquina().vender(venta); 
		
		//this.getMaquina().vender(new Venta(boleto, "24/10/2021 22:00"));
	}
	
	public void run() {
		for (int i = 0; i < 10; i++) {
			this.realizarVenta();
		}
		this.getMaquina().vender(null);
		
	}
	
	
}
