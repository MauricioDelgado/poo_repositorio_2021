import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Online extends PtoVenta {

	public Online(MaquinaFiscal maquina) {
		super(maquina);
		
	}
	
	public void revisarConexion() throws FallaConexion {
		if (ThreadLocalRandom.current().nextInt(100)<15) {
			throw new FallaConexion();
		}
	}
	
	
	
	public void realizarVenta() {
		super.realizarVenta();
		Integer codigo = ThreadLocalRandom.current().nextInt(900)+100;
		System.out.println("CODIGO PARA RETIRAR LA ENTRADA = ELS"+ codigo);
		
	}
	
	
	public void run() {
		for (int i = 0; i < 10; i++) {
			try {
				this.revisarConexion();
				this.realizarVenta();
			} catch (FallaConexion e) {
				System.out.println("FALLO: Fall� la conexi�n con el servidor.");
			}
		}
		this.getMaquina().vender(null);
		
	}
	

}
