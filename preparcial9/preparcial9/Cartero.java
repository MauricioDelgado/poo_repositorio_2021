package preparcial9;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;

public class Cartero extends Thread {

	private HashMap<String, LinkedList<Correspondencia>> aEnviar = new HashMap<String, LinkedList<Correspondencia>>();
	private Integer cpInvalidos;
	private Buzon buzon;

	public Cartero(Buzon buzon) {
		super();
		this.buzon = buzon;
	}

	public HashMap<String, LinkedList<Correspondencia>> getaEnviar() {
		return aEnviar;
	}

	public void setaEnviar(HashMap<String, LinkedList<Correspondencia>> aEnviar) {
		this.aEnviar = aEnviar;
	}

	public Integer getCpInvalidos() {
		return cpInvalidos;
	}

	public void setCpInvalidos(Integer cpInvalidos) {
		this.cpInvalidos = cpInvalidos;
	}

	public Buzon getBuzon() {
		return buzon;
	}

	public void setBuzon(Buzon buzon) {
		this.buzon = buzon;
	}

	public String informeEnvios() {
		Set<String> claves = this.getaEnviar().keySet();
		System.out.println("\nINFORME DE ENV�OS");
		for (String clave : claves) {
			System.out.println(this.getaEnviar().get(clave).size() + " elementos de correspondencia enviados a " + clave);
		}
		System.out.println(this.getCpInvalidos() + " env�os con c�digo postal inv�lido");
		return null;
	}

	public void run() {
		Integer contador = 0;
		this.setCpInvalidos(0);
		while (contador < 3) {
			try {
				Correspondencia obj = this.getBuzon().retirar();
				if (obj == null) {
					contador++;
				} else {
					if (!this.getaEnviar().containsKey(obj.getDestinatario())) {
						LinkedList<Correspondencia> paqueteria = new LinkedList<Correspondencia>();
						paqueteria.add(obj);
						aEnviar.put(obj.getDestinatario(), paqueteria);
					} else {
						aEnviar.get(obj.getDestinatario()).add(obj);
					}
				}
			} catch (CodigoPostalInvalidoException e) {
				System.err.println(e.getMessage());
				this.setCpInvalidos(this.getCpInvalidos()+1);
			}
		}
		informeEnvios();
	}
}