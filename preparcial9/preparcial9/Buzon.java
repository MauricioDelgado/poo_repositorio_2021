package preparcial9;

import java.util.LinkedList;

public class Buzon {

	LinkedList<Correspondencia> correspondencia = new LinkedList<Correspondencia>();
	
	
	public synchronized void depositar(Correspondencia c) {
		while (correspondencia.size() >= 5) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if (c != null) {
			System.out.println("PERSONA DEPOSITA: " + c.toString());
		}
		correspondencia.addLast(c);
		notifyAll();
	}

	public LinkedList<Correspondencia> getCorrespondencia() {
		return correspondencia;
	}

	public void setCorrespondencia(LinkedList<Correspondencia> correspondencia) {
		this.correspondencia = correspondencia;
	}

	public synchronized Correspondencia retirar() throws CodigoPostalInvalidoException{
		while(this.getCorrespondencia().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Correspondencia co = correspondencia.removeFirst();
		if(co!=null) {
			if(!(co.getCodigoPostal().equals("9000"))) {
				throw new CodigoPostalInvalidoException("No se pudo enviar la correspondencia, c�digo inv�lido.");
			}else {
			System.out.println("CARTERO RETIRA: " + co.toString());
		}
		}
	notifyAll();
	return co;
	}
}