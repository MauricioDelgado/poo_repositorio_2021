package preparcial9;

import java.util.Random;

public class Persona implements Runnable {

	private String nombre;
	private Buzon buzon;

	public Persona(Buzon buzon, String nombre) {
		super();
		this.nombre = nombre;
		this.buzon = buzon;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Buzon getBuzon() {
		return buzon;
	}

	public void setBuzon(Buzon buzon) {
		this.buzon = buzon;
	}

	private Correspondencia crearCorrespondencia() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String codigoPostal = null;
		String destinatario = null;
		String direccion = null;
		String texto = null;
		Random r = new Random();
		Integer ran = r.nextInt(100);
		if (ran < 25) {
			destinatario = "Destinatario 0";
			direccion = "Direccion 0";
			texto = "Texto 0";
		} else if (ran > 24 && r.nextInt() < 50) {
			destinatario = "Destinatario 1";
			direccion = "Direccion 1";
			texto = "Texto 1";
		} else if (ran > 49 && r.nextInt() < 75) {
			destinatario = "Destinatario 2";
			direccion = "Direccion 2";
			texto = "Texto 2";
		} else {
			destinatario = "Destinatario 3";
			direccion = "Direccion 3";
			texto = "Texto 3";
		}
		if (new Random().nextInt(100) < 80) {
			codigoPostal = "9000";
		} else {
			codigoPostal = "9001";
		}
		if (new Random().nextInt(100) < 50) {
			Double peso = new Random().nextDouble() * 4 + 1;
			return new Paquete(this.getNombre(), destinatario, direccion, codigoPostal, peso);
		} else {
			return new Carta(this.getNombre(), destinatario, direccion, codigoPostal, texto);
		}
	}

	public void run() {
		for (int i = 0; i < 5; i++) {
			this.getBuzon().depositar(this.crearCorrespondencia());
		}
		this.getBuzon().depositar(null);
	}
}