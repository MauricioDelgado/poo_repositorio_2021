package preparcial9;

public class CodigoPostalInvalidoException extends Exception {

	private static final long serialVersionUID = 1L;

	public CodigoPostalInvalidoException(String message) {
		super(message);
	}
}