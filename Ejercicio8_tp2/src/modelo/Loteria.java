package modelo;

import java.security.SecureRandom;
import java.util.ArrayList;


public class Loteria {
	 
	//atributos
	private ArrayList<Integer> carton;

	//constructor
	public Loteria() {
		this.carton= new ArrayList<Integer>();
	}
	
	//metodos
	public ArrayList<Integer> getCarton() {
		return carton;
	}

	public void setCarton(ArrayList<Integer> carton) {
		this.carton = carton;
	}

	// metodos propios
	public void agregoNumero(Integer num) {
		this.carton.add(num);
	}
		
	public void imprimir( ) {
		for (Integer num : carton) {
			System.out.println(num);
		}
	}
	
}
