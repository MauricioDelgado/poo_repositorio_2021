import java.time.LocalDate;

public class Contratado extends Empleado {

	private Double tarifaHora, cantidadHorasMes;
	
	public Contratado(Integer nro_legajo, String nombre, String puesto, String direccion, String telefono,
			LocalDate fechNacimiento, LocalDate fechaContratacion, Double tarifaHora, Double cantidadHorasMes) {
		super(nro_legajo, nombre, puesto, direccion, telefono, fechNacimiento, fechaContratacion);
		
		this.setTarifaHora(tarifaHora);
		this.setCantidadHorasMes(cantidadHorasMes);
	}

	public Double getTarifaHora() {
		return tarifaHora;
	}

	public void setTarifaHora(Double tarifaHora) {
		this.tarifaHora = tarifaHora;
	}

	public Double getCantidadHorasMes() {
		return cantidadHorasMes;
	}

	public void setCantidadHorasMes(Double cantidadHorasMes) {
		this.cantidadHorasMes = cantidadHorasMes;
	}

	public Double calcularSueldo() {
		return this.getCantidadHorasMes()* this.getTarifaHora(); 
	}

	@Override
	public String toString() {
		return super.toString() + 
				"\nSueldo: " + this.calcularSueldo();
	}
	
	
	
}
