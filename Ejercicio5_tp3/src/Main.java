import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		
		Trabajador maestro = new Trabajador(255, "Pedro", "Maestro", "Calle falsa 1234", "2974569887", LocalDate.of(1993, 04, 11), LocalDate.of(2021, 04, 11), 25000.0, 2000.0, 4000.0, 1000.0); 
		Trabajador maestro2 = new Trabajador(144, "Maria", "Maestro", "Calle aquiles baesa", "2974515287", LocalDate.of(1985, 05, 21), LocalDate.of(2015, 04, 11), 30000.0, 2000.0, 4000.0, 5000.0); 
		Trabajador maestro3 = new Trabajador(132, "Carla", "Maestro", "Calle come aca", "2974569555", LocalDate.of(1990, 04, 11), LocalDate.of(2019, 04, 15), 27000.30, 2000.0, 4000.0, 3330.99); 

		Contratado suplente = new Contratado(159, "Kiko", "Auxiliar", "Av Como alla 123", "2975486521", LocalDate.of(1970, 02, 02),  LocalDate.of(2000, 05, 10), 150.30, 350.0);
		Contratado suplente2 = new Contratado(160, "Chavo", "Limpieza", "Av torta de jamon 12", "2974396544", LocalDate.of(1970, 05, 07),  LocalDate.of(1990, 01, 07), 150.30, 350.0);

		ArrayList<Empleado> personal = new ArrayList<Empleado>(); 
		
		personal.add(maestro);
		personal.add(maestro2);
		personal.add(maestro3);
		personal.add(suplente);
		personal.add(suplente2);
		
		Iterator<Empleado> it =  personal.iterator(); 
		
		 while (it.hasNext()) {
			Empleado empleado = it.next();
			System.out.println("\n");
			System.out.println(empleado.toString());
		}
		
		
		
		

	}

}
