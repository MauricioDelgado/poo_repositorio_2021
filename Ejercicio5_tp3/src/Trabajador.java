import java.time.LocalDate;

public class Trabajador extends Empleado {

	private Double sueldo, retenciones, impuestos, premios; 
	
	
	public Trabajador(Integer nro_legajo, String nombre, String puesto, String direccion, String telefono,
			LocalDate fechNacimiento, LocalDate fechaContratacion, Double sueldo, Double retenciones, Double impuestos, Double premios) {
		super(nro_legajo, nombre, puesto, direccion, telefono, fechNacimiento, fechaContratacion);
		this.setSueldo(sueldo);
		this.setRetenciones(retenciones);
		this.setImpuestos(impuestos);
		this.setPremios(premios);
	}


	public Double getSueldo() {
		return sueldo;
	}


	public void setSueldo(Double sueldo) {
		this.sueldo = sueldo;
	}


	public Double getRetenciones() {
		return retenciones;
	}


	public void setRetenciones(Double retenciones) {
		this.retenciones = retenciones;
	}


	public Double getImpuestos() {
		return impuestos;
	}


	public void setImpuestos(Double impuestos) {
		this.impuestos = impuestos;
	}


	public Double getPremios() {
		return premios;
	}


	public void setPremios(Double premios) {
		this.premios = premios;
	}

	public Double calcularSueldo() {
		return this.getSueldo() - this.getRetenciones() - this.getImpuestos() + this.getPremios(); 
	}


	@Override
	public String toString() {
		return super.toString() + 
				"\nSueldo: " + this.calcularSueldo();
	}
	
	
	
}
