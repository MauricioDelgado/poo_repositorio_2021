import java.time.LocalDate;

public class Empleado {

	private Integer nro_legajo; 
	private String nombre, puesto, direccion, telefono; 
	private LocalDate fechNacimiento, fechaContratacion;
	
	public Empleado(Integer nro_legajo, String nombre, String puesto, String direccion, String telefono,
			LocalDate fechNacimiento, LocalDate fechaContratacion) {
		super();
		this.nro_legajo = nro_legajo;
		this.nombre = nombre;
		this.puesto = puesto;
		this.direccion = direccion;
		this.telefono = telefono;
		this.fechNacimiento = fechNacimiento;
		this.fechaContratacion = fechaContratacion;
	}

	public Integer getNro_legajo() {
		return nro_legajo;
	}

	public void setNro_legajo(Integer nro_legajo) {
		this.nro_legajo = nro_legajo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public LocalDate getFechNacimiento() {
		return fechNacimiento;
	}

	public void setFechNacimiento(LocalDate fechNacimiento) {
		this.fechNacimiento = fechNacimiento;
	}

	public LocalDate getFechaContratacion() {
		return fechaContratacion;
	}

	public void setFechaContratacion(LocalDate fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	} 
	
	public String toString() {
		return "Nombre: " + this.getNombre() +
				"\nPuesto: " +this.getPuesto()+
				"\nNro Legajo: " +this.getNro_legajo()+
				"\nDireccion: " +this.getDireccion()+
				"\nTelefono: " +this.getTelefono()+
				"\nFecha Nacimiento: "	+ this.getFechNacimiento()+
				"\nFecha Contratacion: "+ this.getFechaContratacion();
		
	}

}
