package ejemploconexionbd;


public class Main {
	static UsuarioDAO usuarioDao = new UsuarioDAO();
	public static void main(String[] args) {
		System.out.println("----------- TODOS LOS USUARIOS: -----------");
		imprimirTodos();
		
		System.out.println("----------- INSERTANDO UN ELEMENTO: -----------");
		Integer idInsertado = usuarioDao.insert(new Usuario("Tomas", "1111"));
		imprimirTodos();
		Usuario u = usuarioDao.get(idInsertado);
		u.setContrasenia("1234");
		u.setNombre("Matias");
	
		System.out.println("----------- MODIFICO EL USUARIO NUEVO: -----------");
		usuarioDao.update(u);
		imprimirTodos();
		System.out.println("----------- BORRO EL USUARIO NUEVO: -----------");
		usuarioDao.remove(u);
		imprimirTodos();
	}
	
	public static void imprimirTodos() {
		
		for (Usuario u : usuarioDao.getAll()) {
			System.out.println(u);
		}
	}
}
