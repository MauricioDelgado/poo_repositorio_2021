package vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.Buscar;
import controlador.Controlador;
import javax.swing.JTextField;

import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class VistaBuscador extends JFrame {

	private JPanel contentPane;
	private Buscar controlador;
	private JTextField textBuscar;
	private JTable table;
	public VistaBuscador( Buscar controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 433, 248);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textBuscar = new JTextField();
		textBuscar.setBounds(10, 11, 283, 20);
		textBuscar.addKeyListener(controlador);
		contentPane.add(textBuscar);
		textBuscar.setColumns(10);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(controlador);
		btnBuscar.setBounds(303, 10, 89, 23);
		//contentPane.add(btnBuscar);
		
		Vector columna = new Vector();
		columna.add("Nombre");
		columna.add("Genero");
		columna.add("Email");
		columna.add("Fecha de nacimiento");
		
		table = new JTable(controlador.buscarDatos(null));

		table.setBounds(10, 61, 397, 137);
		contentPane.add(table);
		
	}
	public Buscar getControlador() {
		return controlador;
		
	}
	public void setControlador(Buscar controlador) {
		this.controlador = controlador;
	}
	public JTextField getTextBuscar() {
		return textBuscar;
	}
	public void setTextBuscar(JTextField textBuscar) {
		this.textBuscar = textBuscar;
	}
	public JTable getTable() {
		return table;
	}
	public void setTable(JTable table) {
		this.table = table;
	}
}
