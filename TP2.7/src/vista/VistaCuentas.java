package vista;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controlador.Controlador;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class VistaCuentas extends JFrame {

	private JPanel contentPane;
	private JTextField txtNombre;
	private JTextField txtGenero;
	private JTextField txtEmail;
	private JTextField txtContrasenia;
	private JTextField txtFechaNac;
	private Controlador controlador;


	public VistaCuentas( Controlador controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 433, 248);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNuevaCuenta = new JLabel("Nueva Cuenta");
		lblNuevaCuenta.setFont(new Font("Tahoma", Font.PLAIN, 19));
		lblNuevaCuenta.setBounds(155, 11, 127, 26);
		contentPane.add(lblNuevaCuenta);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 48, 388, 111);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(5, 2, 0, 0));
		
		JLabel lblNewLabel_1 = new JLabel("Nombre:");
		panel.add(lblNewLabel_1);
		
		txtNombre = new JTextField();
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Genero:");
		panel.add(lblNewLabel_2);
		
		txtGenero = new JTextField();
		panel.add(txtGenero);
		txtGenero.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Email: ");
		panel.add(lblNewLabel);
		
		txtEmail = new JTextField();
		txtEmail.setText("");
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Contrase\u00F1a: ");
		panel.add(lblNewLabel_3);
		
		txtContrasenia = new JTextField();
		panel.add(txtContrasenia);
		txtContrasenia.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Fecha de nacimiento: ");
		panel.add(lblNewLabel_4);
		
		txtFechaNac = new JTextField();
		panel.add(txtFechaNac);
		txtFechaNac.setColumns(10);
		
		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.addActionListener(this.getControlador());
		btnNewButton.setBounds(297, 170, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(this.getControlador());
		btnBuscar.setBounds(20, 170, 89, 23);
		contentPane.add(btnBuscar);
	}
	
	public JTextField getTxtNombre() {
		return txtNombre;
	}


	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}


	public JTextField getTxtGenero() {
		return txtGenero;
	}


	public void setTxtGenero(JTextField txtGenero) {
		this.txtGenero = txtGenero;
	}


	public JTextField getTxtEmail() {
		return txtEmail;
	}


	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}


	public JTextField getTxtContrasenia() {
		return txtContrasenia;
	}


	public void setTxtContrasenia(JTextField txtContrasenia) {
		this.txtContrasenia = txtContrasenia;
	}


	public JTextField getTxtFechaNac() {
		return txtFechaNac;
	}


	public void setTxtFechaNac(JTextField txtFechaNac) {
		this.txtFechaNac = txtFechaNac;
	}


	public Controlador getControlador() {
		return controlador;
	}


	public void setControlador(Controlador controlador) {
		this.controlador = controlador;
	}
}
