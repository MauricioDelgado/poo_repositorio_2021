package modelo;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Banco {

	String nombre;
	HashMap<Persona, Double> cuentas;

	public Banco(String nombre) {
		super();
		this.nombre = nombre;
		setCuentas(new HashMap<>());
	}

	public Boolean agregarCuenta(Persona persona, Double saldo) {
		if (!getCuentas().containsKey(persona)) {
			getCuentas().put(persona, saldo);
			return true;
		}
		return false;
	}

	public Boolean eliminarCuenta(Persona persona) {
		if (getCuentas().containsKey(persona)) {
			getCuentas().remove(persona);
			return true;
		}
		return false;

	}

	public TableModel buscar(String valor) {

		DefaultTableModel tmodel = new DefaultTableModel();
		Object[] encabezado = { "nombre", "Genero", "Email", "Fecha de nacimiento" };
		tmodel.setColumnIdentifiers(encabezado);

		if (valor != null) {
			String pattern = ".*" + valor + ".*";

			Iterator<Persona> iterator = getCuentas().keySet().iterator();
			while (iterator.hasNext()) {
				Persona unaPersona = iterator.next();
				boolean matchNombre = Pattern.matches(pattern, unaPersona.getNombre());
				boolean matchEmail = Pattern.matches(pattern, unaPersona.getEmail());

				if (matchNombre | matchEmail) {
					Object[] fila = new Object[] { unaPersona.getNombre(), unaPersona.getGenero(),
													unaPersona.getEmail(), unaPersona.getFechaNacimiento() };
					tmodel.addRow(fila);
				}
			}
		} else {
			Iterator<Persona> iterator = getCuentas().keySet().iterator();
			while (iterator.hasNext()) {
				Persona unaPersona = iterator.next();
				Object[] fila = new Object[] { unaPersona.getNombre(), unaPersona.getGenero(),
												unaPersona.getEmail(), unaPersona.getFechaNacimiento() };
				tmodel.addRow(fila);
			}
		}

		return tmodel;
	}

	public void listarClientes() {

		if (getCuentas().isEmpty()) {
			System.out.println("No existen cuentas.");
		} else {

			Iterator<Persona> iterator = getCuentas().keySet().iterator();
			while (iterator.hasNext()) {
				Persona key = iterator.next();
				System.out.println("Nombre cliente: " + key.getNombre() + " Saldo: " + getCuentas().get(key));
			}
		}
	}

	public Double getSaldo(Persona persona) {
		if (getCuentas().containsKey(persona)) {
			return getCuentas().get(persona);
		} else {
			System.out.println("No existe cuenta de esta persona");
			return 0.0;
		}
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	private HashMap<Persona, Double> getCuentas() {
		return cuentas;
	}

	private void setCuentas(HashMap<Persona, Double> cuentas) {
		this.cuentas = cuentas;
	}

}
