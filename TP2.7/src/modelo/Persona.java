package modelo;
import java.time.LocalDate;
import java.util.ArrayList;

public class Persona {

	String nombre;
	LocalDate fechaNacimiento;
	String genero;
	String email;
	String contraseña; 
	


	public Persona(String nombre, LocalDate fechaNacimiento, String genero, String email, String contraseña) {
		super();
		this.nombre = nombre;
		this.fechaNacimiento = fechaNacimiento;
		this.genero = genero;
		this.email = email;
		this.contraseña = contraseña;
		
	}
	

	@Override
	public String toString() {
		return "nombre: "+getNombre()+"\n"+
				"fecha de nacimiento: "+getFechaNacimiento()+"\n"+
				"genero: "+getGenero()+"\n"+
				"email: "+getEmail()+"\n";
				
	}


	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}


}
