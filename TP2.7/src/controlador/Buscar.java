package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import modelo.Banco;
import vista.VistaBuscador;

public class Buscar implements ActionListener, KeyListener {
	private Banco modelo;

	private VistaBuscador vista;

	public Buscar(Banco modelo) {
		super();
		this.modelo = modelo;

		vista = new VistaBuscador(this);
		vista.setVisible(true);
	}

	@Override
		public void actionPerformed(ActionEvent arg0) {
			JButton btn = (JButton) arg0.getSource();
			if (btn.getText().equals("Buscar")) {
				getVista().getTable().setModel(this.buscarDatos(getVista().getTextBuscar().getText()));
				}
			
		}

	public TableModel buscarDatos(String valor) {
		
		return getModelo().buscar(valor);
	}

	public Banco getModelo() {
		return modelo;
	}

	public void setModelo(Banco modelo) {
		this.modelo = modelo;
	}

	public VistaBuscador getVista() {
		return vista;
	}

	public void setVista(VistaBuscador vista) {
		this.vista = vista;
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		getVista().getTable().setModel(this.buscarDatos(getVista().getTextBuscar().getText()));
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
