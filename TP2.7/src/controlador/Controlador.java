package controlador;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;

import javax.swing.JButton;

import modelo.Banco;
import modelo.Persona;
import vista.VistaCuentas;

public class Controlador implements ActionListener {
	private Banco modelo ;
	
	private VistaCuentas vista;

	public Controlador() {
		super();
		modelo = new Banco("Banco POO");
		
		 vista = new VistaCuentas(this);

	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		JButton btn = (JButton) arg0.getSource();
		if (btn.getText().equals("Guardar")) {
			String nombre = getVista().getTxtNombre().getText();
			LocalDate fechaNacimiento = LocalDate.parse(getVista().getTxtFechaNac().getText());
			String genero = getVista().getTxtGenero().getText();
			String email = getVista().getTxtEmail().getText();
			String contraseņa = getVista().getTxtContrasenia().getText();
			Persona persona= new Persona(nombre, fechaNacimiento, genero, email, contraseņa);
			this.getModelo().agregarCuenta(persona, 0.0);
			
			this.getModelo().listarClientes();
			
		}
		
		if(btn.getText().equals("Buscar")){
			new Buscar(this.getModelo());
		}
		
	}

	

	public Banco getModelo() {
		return modelo;
	}

	public void setModelo(Banco modelo) {
		this.modelo = modelo;
	}

	public VistaCuentas getVista() {
		return vista;
	}

	public void setVista(VistaCuentas vista) {
		this.vista = vista;
	}



	

}
