package main;

import java.awt.EventQueue;

import controlador.Controlador;

public class BancoMain {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					Controlador controlador = new Controlador();
					controlador.getVista().setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

}
