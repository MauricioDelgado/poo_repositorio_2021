import java.time.LocalDate;
import java.util.Scanner;

public class Coleccion {
	
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
		Persona persona = new Persona("Mauricio", "M", "mauri@unEmail.com",
				LocalDate.of(1993, 04, 11), "z3pp3");
		
		VideoJuego juego1 = new VideoJuego();
		VideoJuego juego2 = new VideoJuego();
		VideoJuego juego3 = new VideoJuego();
		System.out.println(persona.toString());
		
		juego1 = new VideoJuego("Packman", "1980", "Namco", Genero.ARCADE);
		juego2 = new VideoJuego("Doom", "1993", "Id software/ Nerve Software", Genero.SHOOTER);
		juego3 = new VideoJuego("Donkey Kong", "1981", "Nintendo", Genero.ARCADE);
				
		persona.agregarJuego(juego1);
		persona.agregarJuego(juego2);
		persona.agregarJuego(juego3);
		
		persona.listarJuegos();
		
		persona.quitarJuego(juego3);
		
		persona.listarJuegos();
		
		System.out.println("INGRESE JUEGO QUE QUIERE ELIMINAR: ");
		String pregunta = entrada.next();
		
		if(persona.quitarJuego(pregunta)) {
			System.out.println("Juego eliminado");
		}else {
			System.out.println("No se puede eliminar el juego");
		}
		
		persona.listarJuegos();
		
		
	}
}
