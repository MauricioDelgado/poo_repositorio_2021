import java.time.LocalDate;
import java.util.ArrayList;


public class Persona {
	
	// ATRIBUTOS
		private String nombre;
		private String genero;
		private String email;
		private LocalDate fecha_nacimiento;
		private String contrasenia;
		private ArrayList<VideoJuego> juegos; 
		

	// CONSTRUCTOR SOBRECARGADO
		public Persona(String nombre,String genero,String email, LocalDate fecha_nacimiento, String contrasenia) {
			
			this.setNombre(nombre);
			this.setGenero(genero);
			this.setEmail(email);
			this.setFecha_nacimiento(fecha_nacimiento);
			this.setContrasenia(contrasenia);
			//this.setJuegos(juegos);
			this.juegos = new ArrayList<VideoJuego>();
			
		}
			

	// METODOS
		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getGenero(String genero) {
			return genero;
		}

		public void setGenero(String genero) {
			this.genero = genero;
		}
		
		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public LocalDate getFecha_nacimiento() {
			return fecha_nacimiento;
		}

		public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
			this.fecha_nacimiento = fecha_nacimiento;
		}
		
		public String getContrasenia() {
			return contrasenia;
		}

		public void setContrasenia(String contrasenia) {
			this.contrasenia = contrasenia;
		}

		public ArrayList<VideoJuego> getJuegos() {
			return juegos;
		}

		public void setJuegos(ArrayList<VideoJuego> juegos) {
			this.juegos = juegos;
		}

		@Override
		public String toString() {
			return "NOMBRE: " + nombre +  
				   "\nFECHA DE NACIMIENTO: " + fecha_nacimiento +
				   "\nGENERO: " + genero +
				   "\nEMAIL: " + email ;
		}

		void agregarJuego(VideoJuego juegos) {
			getJuegos().add(juegos); 
		}
		
		
		public void quitarJuego(VideoJuego juego) {
			this.getJuegos().remove(juego);
		}
		
		public Boolean quitarJuego(String nombre) {
			for (int i = 0; i < juegos.size(); i++) {
				if (this.getJuegos().get(i).getNombre().equalsIgnoreCase(nombre)) {
					this.getJuegos().remove(i);
					return true;
				}
				
			}
			return null;
	   }
			
		public void listarJuegos() {
			for (int i = 0; i < juegos.size(); i++) {
				System.out.println(this.getJuegos().get(i).toString());
			}
				
				//System.out.println(this.getJuegos().toString());
			}
			
				
			
			
			
					
					
				
}
		















