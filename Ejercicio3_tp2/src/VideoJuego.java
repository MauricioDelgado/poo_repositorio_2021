

public class VideoJuego {
	
	private String nombre, anio, desarrollador; 
	private Genero genero = Genero.NINGUNO;
	
// METODO POR DEFECTO	
	public VideoJuego() {
		this("", "", "", Genero.NINGUNO);
	}
	
// METODO SOBRERCAGADO
	public VideoJuego(String nombre, String anio, String desarrollador, Genero genero) {
		this.setNombre(nombre);
		this.setAnio(anio);
		this.setDesarrollador(desarrollador);
		this.setGenero(genero);
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}


	public String getDesarrollador() {
		return desarrollador;
	}

	public void setDesarrollador(String desarrollador) {
		this.desarrollador = desarrollador;
	}
	
	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}
    
	
	@Override
	public String toString() {
		return "NOMBRE: " + nombre +  
			   ", " +"A�O: " + anio +
			   ", " +"DESARROLLADOR: " + desarrollador +
			   ", " + "GENERO: " + genero ;
	}
}

