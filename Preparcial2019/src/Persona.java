import java.util.Random;

public class Persona implements Runnable {

	private String nombre;
	private Buzon buzon;

	public Persona(Buzon buzon, String nombre) {
		this.buzon = buzon;
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Buzon getBuzon() {
		return buzon;
	}

	public void setBuzon(Buzon buzon) {
		this.buzon = buzon;
	}

//////////////////////////////////////////////////////////

	public Correspondencia crearCorrespondencia() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		String codigoPostal = "9000";
		Integer num = new Random().nextInt(3)+1; 
		
		if (new Random().nextInt() <= 20) {
			codigoPostal = "9001";
		}
		
		if (new Random().nextInt() <= 50) {
			
			return new Paquete(this.getNombre(), "destinatario "+ num, "direccion" +num, codigoPostal , new Random().nextDouble()*4+1);
		}else{
			return new Carta(this.getNombre(), "destinatario "+ num, "direccion" +num, codigoPostal , num.toString());

		}
	}

	@Override
	public void run() {

		for (int i = 0; i < 5; i++) {
			this.getBuzon().depositar(this.crearCorrespondencia());
		}
		this.getBuzon().depositar(null);
	}

}
