
public class Carta extends Correspondencia {
	private String texto;
	
	public Carta(String remitente, String destinatario, String direccion, String codigoPostal, String texto) {
		super(remitente, destinatario, direccion, codigoPostal);
		this.texto = texto;
		}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	@Override
	public String toString() {
		return "CARTA --> "+super.toString() + ",texto= " + texto+ "]";
	}

	
	
}
