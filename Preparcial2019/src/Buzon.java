import java.util.LinkedList;

public class Buzon {

	private LinkedList<Correspondencia> correspondencia = new LinkedList<Correspondencia>();

	public LinkedList<Correspondencia> getCorrespondencia() {
		return correspondencia;
	}

	public void setCorrespondencia(LinkedList<Correspondencia> correspondencia) {
		this.correspondencia = correspondencia;
	}

/////////////////////////////////////////////////////////

	public synchronized void depositar(Correspondencia c) {

		while (this.correspondencia.size() >= 5) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (c != null) {
			System.out.println("PERSONA DEPOSITA: " + c.toString());
		}

		this.getCorrespondencia().addLast(c);
		notifyAll();
	}

	public synchronized Correspondencia retirar() throws CodigoPostalInvalido {
		
		while (this.getCorrespondencia().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		Correspondencia correo = correspondencia.removeFirst();
		if (correo != null) {
			
			if(correo.getCodigoPostal().equals("9001")) {
				throw new CodigoPostalInvalido(); 
			}else {
				System.out.println("CARTERO RETIRA: " + correo.toString());
			}
		}
		
		notifyAll();
		return correo;

	}
}
