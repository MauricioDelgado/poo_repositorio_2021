
public class Correspondencia {

	private String remitente, destinatario, direccion, codigoPostal;

	public Correspondencia(String remitente, String destinatario, String direccion, String codigoPostal) {
		super();
		this.remitente = remitente;
		this.destinatario = destinatario;
		this.direccion = direccion;
		this.codigoPostal = codigoPostal;
	}

	public String getRemitente() {
		return remitente;
	}

	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	
	/////////////////////////////////////////////
	
	@Override
	public String toString() {
		return " [remitente=" + remitente + ", destinatario=" + destinatario + ", direccion=" + direccion
				+ ", codigoPostal=" + codigoPostal ;
	} 
	
	
}
