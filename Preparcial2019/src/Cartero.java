import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Cartero extends Thread{

	private HashMap<String , LinkedList<Correspondencia>> aEnviar = new HashMap<String , LinkedList<Correspondencia>>(); 
	private Integer cpInvalidos = 0;
	private Buzon buzon;
	
	
	public Cartero(Buzon buzon) {
		super();
		this.buzon = buzon;
	}
	
	
	public HashMap<String, LinkedList<Correspondencia>> getaEnviar() {
		return aEnviar;
	}
	public void setaEnviar(HashMap<String, LinkedList<Correspondencia>> aEnviar) {
		this.aEnviar = aEnviar;
	}
	public Integer getCpInvalidos() {
		return cpInvalidos;
	}
	public void setCpInvalidos(Integer cpInvalidos) {
		this.cpInvalidos = cpInvalidos;
	}
	public Buzon getBuzon() {
		return buzon;
	}
	public void setBuzon(Buzon buzon) {
		this.buzon = buzon;
	} 
	
	///////////////////////////////////////////////////
	public String informeEnvios() {
	
		for (Map.Entry<String, LinkedList<Correspondencia>> objeto : aEnviar.entrySet()) {
			System.out.println(objeto.getValue().size() + " elementos de correspondencia enviados a "+ objeto.getKey());
		}
		
		System.out.println(this.getCpInvalidos() +" envios con codigo postal nulo");
		return null;
		
	}
	
	public void run() {
		int contador =0; 
		
		while(contador < 3) {
			try {
				Correspondencia correo = this.getBuzon().retirar();
				if (correo == null) {
					contador++; 
				} else {
					if (!this.getaEnviar().containsKey(correo.getDestinatario())) {
						LinkedList<Correspondencia> envio = new LinkedList<Correspondencia>();
						envio.add(correo); 
						this.getaEnviar().put(correo.getDestinatario(), envio);
						
					}else {
							this.getaEnviar().get(correo.getDestinatario()).add(correo);
						
					}
					
				}
			} catch (CodigoPostalInvalido e) {
				this.setCpInvalidos(this.getCpInvalidos()+1); 
				System.out.println("NO SE PUDO ENVIAR LA CORRESPONDENCIA, CODIGO POSTAL INVALIDO");
			}
		}
		
		System.out.println("INFORME DE ENVIOS:");
		this.informeEnvios(); 
	}
}
