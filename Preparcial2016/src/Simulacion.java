

public class Simulacion {

	public static void main(String[] args) {

		Deposito deposito = new Deposito(); 
		new Agricola("Agricola", deposito).start(); 
		new Ganadero("Ganadero", deposito).start();
		new Comercio(deposito).start();
		
	}

}
