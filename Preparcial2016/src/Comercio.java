import java.util.ArrayList;

public class Comercio extends Thread{

	private ArrayList<Lote> colLote = new ArrayList<Lote>() ;
	private Deposito deposito;
	
	public Comercio (Deposito deposito) {
		this.setDeposito(deposito);
	}

	public Deposito getDeposito() {
		return deposito;
	}

	public void setDeposito(Deposito deposito) {
		this.deposito = deposito;
	}

	public ArrayList<Lote> getColLote() {
		return colLote;
	}

	public void setColLote(ArrayList<Lote> colLote) {
		this.colLote = colLote;
	}
	
	public void informe() {
		Integer total_ganadero = 0;
		Integer total_agricola = 0;
		
		for (Lote lote : colLote) {
			if (lote.getNomnreProductor().equalsIgnoreCase("Ganadero")) {
				total_ganadero = total_ganadero+ 5000; 
			}else {
				total_agricola = total_agricola+ 500; 
			}
		}
		
		System.out.println("$" + total_agricola + " de ingresos provenientes de las ventas de productos Agricolas y $" + total_ganadero + " del ganadero");
		
	}
	
	public void run() {
		Integer contador = 0;
		
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		while(contador < 2) {
			Lote unLote = this.getDeposito().vender();
			if(unLote == null) {
				contador++;
			}else {
				this.getColLote().add(unLote);
			}
		}
		System.out.println("\nINFORME:");
		this.informe();
	}
	
	
}

