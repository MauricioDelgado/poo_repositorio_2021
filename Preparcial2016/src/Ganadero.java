import java.util.Random;

public class Ganadero extends Thread implements Productor{

	private String nombre;
	private Deposito depositoComercio;
	
	public Ganadero(String nombre, Deposito depositoComercio) {
		super();
		this.nombre = nombre;
		this.depositoComercio = depositoComercio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Deposito getDepositoComercio() {
		return depositoComercio;
	}

	public void setDepositoComercio(Deposito depositoComercio) {
		this.depositoComercio = depositoComercio;
	}
	

	@Override
	public Lote producir() {
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return new Lote(new Random().nextInt(5)+1, "carne", System.currentTimeMillis(), this.getNombre());
		
	}

	@Override
	public void vender(Lote unLote) throws RobotException {
		if (new Random().nextInt() < 50) {
			throw new RobotException();
		}
		this.getDepositoComercio().depositar(unLote);
		
		
	}

	
	public void run() {

		for (int i = 0; i < 5; i++) {
			try {
				this.vender(this.producir());
			} catch (RobotException e) {
				System.out.println("ROBO: (" +this.getNombre()+ ")" + "HUBO UN ROBO AL DEPOSITAR !");
			}
		}
		this.getDepositoComercio().depositar(null);
			
		
	}


}
