
public class Lote {
	private Integer unidades; 
	private String descripcion;
	private long timeStamp;
	private String nombreProductor;
	
	public Lote(Integer unidades, String descripcion, long timeStamp, String nomnreProductor) {
		super();
		this.unidades = unidades;
		this.descripcion = descripcion;
		this.timeStamp = timeStamp;
		this.nombreProductor = nomnreProductor;
	}

	public Integer getUnidades() {
		return unidades;
	}

	public void setUnidades(Integer unidades) {
		this.unidades = unidades;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getNomnreProductor() {
		return nombreProductor;
	}

	public void setNomnreProductor(String nomnreProductor) {
		this.nombreProductor = nomnreProductor;
	}

	@Override
	public String toString() {
		return "Lote [unidades=" + unidades + ", descripcion=" + descripcion + ", timeStamp=" + timeStamp
				+ ", nomnreProductor=" + nombreProductor + "]";
	} 
	
	
	
	
	
}
