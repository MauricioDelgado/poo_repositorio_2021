
public interface Productor {
	public Lote producir(); 
	public void vender(Lote unLote) throws RobotException; 
}
