import java.util.LinkedList;

public class Deposito {

	private LinkedList<Lote> colLote = new LinkedList<Lote>();

	public Deposito() {
		super(); 
		this.setColLote(colLote);
	}
	

	public LinkedList<Lote> getColLote() {
		return colLote;
	}
	
	public void setColLote(LinkedList<Lote> colLote) {
		this.colLote = colLote;
	}
	
	public synchronized  Lote vender() {
		while (colLote.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		Lote unLote = colLote.removeFirst();
		if (unLote != null) {
			System.out.println("VENTA:" + unLote.toString());
		}
		
		notifyAll();
		return unLote;
	}
	
	public synchronized void depositar(Lote unLote) {
		while (this.colLote.size() >= 3) {
			 try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		if (unLote != null) {
			System.out.println("DEPOSITO: " + unLote.toString());
		}
		this.getColLote().add(unLote);
		notifyAll();
	}
	
}
