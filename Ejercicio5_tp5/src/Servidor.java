import java.util.LinkedList;

public class Servidor {
 
	private LinkedList<Paquete_Datos> colaPaquetes = new LinkedList<Paquete_Datos>(); 
	
	public synchronized void almacenar(Paquete_Datos unPaquete) {
		
		while (colaPaquetes.size() >= 3) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		
		if (unPaquete != null) {
			System.out.println("ALMACENO: " + unPaquete.toString());
		}
		colaPaquetes.add(unPaquete); 
		notifyAll();
	}
	
	public synchronized Paquete_Datos consultar() {
		
		while (colaPaquetes.isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
		
		Paquete_Datos unPaquete = colaPaquetes.removeFirst();
		if (unPaquete != null) {
			System.out.println("CONSULTO: " + unPaquete.toString());
		}
		notifyAll();
		return unPaquete;
		
	}

	
}
