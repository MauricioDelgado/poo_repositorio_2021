import java.util.Random;

public class Boya extends Thread{

	private String nombre; 
	private Servidor servidor; 
	private Termometro termometro; 
	private Anemometro anemometro;
	
	public Boya(String nombre, Servidor servidor) {
		super();
		this.nombre = nombre;
		this.servidor = servidor;
		this.termometro = new Termometro();
		this.anemometro = new Anemometro();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public Termometro getTermometro() {
		return termometro;
	}

	public void setTermometro(Termometro termometro) {
		this.termometro = termometro;
	}

	public Anemometro getAnemometro() {
		return anemometro;
	}

	public void setAnemometro(Anemometro anemometro) {
		this.anemometro = anemometro;
	} 
	
	private void enviar(Paquete_Datos paquete) throws Mensaje_excepcion {
		try {
			Thread.sleep(5000);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (new Random().nextInt() < 40) {
			throw new Mensaje_excepcion();
		}
		this.getServidor().almacenar(paquete);
		
	}
	
	private Paquete_Datos generarPaquete () {
		return new Paquete_Datos(this.getNombre(), this.getTermometro().sensar(), this.getAnemometro().sensar(), System.currentTimeMillis());
		
	}
	
	public void run() {
		for (int i = 0; i < 5; i++) {
			try {
				this.enviar(this.generarPaquete());
			} catch (Mensaje_excepcion e) {
				System.out.println("ERROR: " + this.getNombre() + " Hubo un a falla en la comunicacion, se perdio el paquete");
				//e.printStackTrace();
			}
		}
		this.getServidor().almacenar(null);
	}
	
}
