import java.util.Iterator;
import java.util.LinkedList;

public class Cientifico extends Thread{

	private Servidor servidor; 
	public LinkedList<Paquete_Datos> colaPaquetes;
	
	public Cientifico(Servidor servidor) {
		super();
		this.servidor = servidor;
		this.colaPaquetes = new LinkedList<>();
	}

	public Servidor getServidor() {
		return servidor;
	}

	public void setServidor(Servidor servidor) {
		this.servidor = servidor;
	}

	public LinkedList<Paquete_Datos> getColaPaquetes() {
		return colaPaquetes;
	}

	public void setColaPaquetes(LinkedList<Paquete_Datos> colaPaquetes) {
		this.colaPaquetes = colaPaquetes;
	}
	
	private void promediar(String nombreBoya, String tipo_dato) {
		Double suma = 0.0; 
		Integer total = 0; 
		
		for (Paquete_Datos paquete : colaPaquetes) {
			if(paquete.getNombreBoya().equals(nombreBoya)) {
				if (tipo_dato.equalsIgnoreCase("temperatura")) {
					suma = suma + paquete.getTemperatura();
				}else {
					suma = suma + paquete.getVelocidadViento();
				}
				total++;
			}
		}
		System.out.println("El promedio de" + tipo_dato +" para la Boya " + nombreBoya +" es :" + suma/total);	
	}

	public void run() {
		Integer contador = 0; 
		while(contador < 2) {
			Paquete_Datos paquete = this.getServidor().consultar(); 
			if (paquete == null) {
				contador++;
			}else {
				this.getColaPaquetes().add(paquete);
			}
		}
		System.out.println("\nINFORME DE PROMEDIOS:");
		this.promediar("CIMAR-1", "temperatura");
		this.promediar("CIMAR-1", "velocidad de Viento");
		this.promediar("CIMAR-2", "temperatura");
		this.promediar("CIMAR-2", "velocidad de Viento");
		
	}
	
	
}

