
public abstract class Animal {
	// atributos
	private String raza;
	private String ubicacion; 
	private String tipo_comida;  
	private String tamanio;
	private Integer edad;
	
	// constructor por defecto
	public Animal() {
		this("", "", "", "", 0);
	}
	
	// constructor sobrecargado
	public Animal(String raza, String ubicacion, String tipo_comida, String tamanio, Integer edad) {
		super();
		this.raza = raza;
		this.ubicacion = ubicacion;
		this.tipo_comida = tipo_comida;
		this.tamanio = tamanio;
		this.edad = edad;
	}
	
	// metodos
	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getTipo_comida() {
		return tipo_comida;
	}

	public void setTipo_comida(String tipo_comida) {
		this.tipo_comida = tipo_comida;
	}

	public String getTamanio() {
		return tamanio;
	}

	public void setTamanio(String tamanio) {
		this.tamanio = tamanio;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}
	
	
	// metodos propios
	 public String obtenerAlimento() {
		 return " esta comiendo";
	 }
	
	public String emitirSonido() {
		 return   this.getRaza()+  " esta emitiendo sonido";
	 }

	
}
