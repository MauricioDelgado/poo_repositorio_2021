
public class Peces extends Animal {
 // atributos
	private String color_escama;
	private Integer cantidad_aletas;
	
	// constructores
	public Peces() {
		
	}

	public Peces(String raza, String ubicacion, String tipo_comida, String tamanio, Integer edad, String color_escama, Integer cantidad_aletas ) {
		super(raza, ubicacion, tipo_comida, tamanio, edad);
		this.setCantidad_aletas(cantidad_aletas);
		this.setColor_escama(color_escama);
	}
	

	//metodos
	public String getColor_escama() {
		return color_escama;
	}

	public void setColor_escama(String color_escama) {
		this.color_escama = color_escama;
	}

	public Integer getCantidad_aletas() {
		return cantidad_aletas;
	}

	public void setCantidad_aletas(Integer cantidad_aletas) {
		this.cantidad_aletas = cantidad_aletas;
	}

	@Override
	public String obtenerAlimento() {
		return "El animal " + getRaza()+ super.obtenerAlimento() + " "+getTipo_comida();
	}

	@Override
	public String emitirSonido() {
		return "no se escucha al animal";
	}
	
	
	
		

}
