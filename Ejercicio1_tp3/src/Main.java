
public class Main {

	public static void main(String[] args) {
		
		Peces salmon = new Peces("Salmon", "Costa Argentina", "insectos", "mediano", 3, "azul oscuro", 5);
		Aves paloma = new Aves("Paloma", "Costa Argentina", "insectos", "mediano", 3, "blanca y negra");
		
		
		System.out.println(salmon.emitirSonido());
		System.out.println(salmon.obtenerAlimento());
		
		System.out.println(paloma.emitirSonido());
		System.out.println(paloma.obtenerAlimento());
	}

}
