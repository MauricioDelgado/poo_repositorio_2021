
public class Aves extends Animal {

	private String color_plumas;
	
	public Aves() {
		
	}

	public Aves(String raza, String ubicacion, String tipo_comida, String tamanio, Integer edad, String color_plumas) {
		super(raza, ubicacion, tipo_comida, tamanio, edad);
		this.setColor_plumas(color_plumas);
	}

	public String getColor_plumas() {
		return color_plumas;
	}

	public void setColor_plumas(String color_plumas) {
		this.color_plumas = color_plumas;
	}

	@Override
	public String obtenerAlimento() {
		return this.getRaza() + super.obtenerAlimento(); 
		// es correcto hacer esto con el this.getRaza o 
		//se deberia hacer como en el emitir sonido de clase Animal?
	}

	@Override
	public String emitirSonido() {
		return super.emitirSonido();
	}

	

}
