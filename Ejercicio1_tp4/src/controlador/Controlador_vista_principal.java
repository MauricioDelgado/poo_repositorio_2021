package controlador;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import vista.Vista_principal;

public class Controlador_vista_principal implements  ChangeListener{

	
	private Vista_principal vp; 
	
	public Controlador_vista_principal(Vista_principal vp){
		this.setVp(vp);
	}
	
	public Vista_principal getVp() {
		return vp;
	}

	public void setVp(Vista_principal vp) {
		this.vp = vp;
	}
	
	public void stateChanged (ChangeEvent e) {
		JCheckBox texto = (JCheckBox) e.getSource(); 
		JCheckBox texto2 = (JCheckBox) e.getSource();
		//lentes
		if (texto.getText().equals("LENTES") && this.getVp().getCBLentes().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/1000.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}else {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/0000.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		//pelo
		if (texto.getText().equals("PELO")&& this.getVp().getCBPelo().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/0010.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		} 
		// menton
		if (texto.getText().equals("MENTON")&& this.getVp().getCBMenton().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/0100.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		} 
		// dientes
		if (texto.getText().equals("DIENTES")&& this.getVp().getCBDientes().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/0001.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		} 
		
		//lentes y menton
		if (this.getVp().getCBLentes().isSelected() &&  this.getVp().getCBMenton().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/1100.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		
		//lentes y pelo
		if (this.getVp().getCBLentes().isSelected() &&  this.getVp().getCBPelo().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/1010.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		//lentes y dientes
		if (this.getVp().getCBLentes().isSelected() &&  this.getVp().getCBDientes().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/1101.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		
		//menton y pelo 
		if (this.getVp().getCBPelo().isSelected() &&  this.getVp().getCBMenton().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/1010.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		//menton y dientes
		if (this.getVp().getCBDientes().isSelected() &&  this.getVp().getCBMenton().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/0101.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		//pelo y dientes 
		if (this.getVp().getCBPelo().isSelected() &&  this.getVp().getCBDientes().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/0011.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		
		//todas las casillas
		if (this.getVp().getCBLentes().isSelected() &&  this.getVp().getCBMenton().isSelected() && this.getVp().getCBPelo().isSelected() && this.getVp().getCBDientes().isSelected()) {
			this.getVp().getLblImagen().setIcon(new ImageIcon(new ImageIcon(Controlador_vista_principal.class.getResource("/imagenes/1111.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		}
		
		
		
	}

	
	
	
	
	
	
	
	
	
	
	
	
	


	
	
}
