package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.Controlador_vista_principal;

import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Vista_principal extends JFrame {

	
	private JPanel contentPane;
	private JCheckBox CBLentes;
	private JCheckBox CBMenton;
	private JCheckBox CBPelo;
	private JCheckBox CBDientes;
	private JLabel lblImagen;

	/**
	 * Create the frame.
	 */
	public Vista_principal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblImagen = new JLabel("Imagen");
		lblImagen.setIcon(new ImageIcon(new ImageIcon(Vista_principal.class.getResource("/imagenes/0000.gif")).getImage().getScaledInstance(187, 160, Image.SCALE_DEFAULT)));
		lblImagen.setBounds(202, 44, 187, 160);
		contentPane.add(lblImagen);
		
		CBLentes = new JCheckBox("LENTES");
		CBLentes.setBounds(23, 44, 97, 23);
		contentPane.add(CBLentes);
		CBLentes.addChangeListener(new Controlador_vista_principal(this));
		
		CBMenton = new JCheckBox("MENTON");
		CBMenton.setBounds(23, 85, 97, 23);
		contentPane.add(CBMenton);
		CBMenton.addChangeListener(new Controlador_vista_principal(this));
		
		CBPelo = new JCheckBox("PELO");
		CBPelo.setBounds(23, 128, 97, 23);
		contentPane.add(CBPelo);
		CBPelo.addChangeListener(new Controlador_vista_principal(this));
		
		CBDientes = new JCheckBox("DIENTES");
		CBDientes.setBounds(23, 171, 97, 23);
		contentPane.add(CBDientes);
		CBDientes.addChangeListener(new Controlador_vista_principal(this));
	}

	public JCheckBox getCBLentes() {
		return CBLentes;
	}

	public void setCBLentes(JCheckBox cBLentes) {
		CBLentes = cBLentes;
	}

	public JCheckBox getCBMenton() {
		return CBMenton;
	}

	public void setCBMenton(JCheckBox cBMenton) {
		CBMenton = cBMenton;
	}

	public JCheckBox getCBPelo() {
		return CBPelo;
	}

	public void setCBPelo(JCheckBox cBPelo) {
		CBPelo = cBPelo;
	}

	public JCheckBox getCBDientes() {
		return CBDientes;
	}

	public void setCBDientes(JCheckBox cBDientes) {
		CBDientes = cBDientes;
	}

	public JLabel getLblImagen() {
		return lblImagen;
	}

	public void setLblImagen(JLabel lblImagen) {
		this.lblImagen = lblImagen;
	}
	
	
	
}
