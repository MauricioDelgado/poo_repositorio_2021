
public class Notificacion_factory {
	
	
	public Notificacion crearNotificacion(String metodo) 
	{
		switch (metodo) {
		case "SMS":
			return new Sms_notificacion(); 
		case "Email":
			return new Email_notificacion(); 
		case "Push":
			return new Push_notificacion(); 
		default: return null;
		}
	} 
}
