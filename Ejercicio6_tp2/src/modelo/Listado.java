package modelo;

import java.util.ArrayList;

public class Listado {
	private ArrayList<Persona> lista;

	public Listado(ArrayList<Persona> lista) {
		this.setLista(lista);
	}

	public ArrayList<Persona> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Persona> lista) {
		this.lista = lista;
	}

	public Boolean agregarPersona (Persona persona) {
		return lista.add(persona);
	}
	
	public Boolean eliminarPersona(Persona pesona) {
		return lista.remove(pesona);
	}
}
