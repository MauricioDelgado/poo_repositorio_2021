package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.FlowLayout;
import javax.swing.border.CompoundBorder;

public class VistaTabla extends JFrame {

	private JPanel contentPane;
	private DefaultTableModel modeloTabla;
	private JTable table;
	/**
	 * Create the frame.
	 */
	public VistaTabla() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 303);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 674, 195);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 664, 195);
		scrollPane.setViewportView(table);
		panel.add(scrollPane);
		
		//
		String header[] = {"NOMBRE", "GENERO","EMAIL", "FECHA DE NACIMIENTO", "CONTRASEŅA"};
		
		this.setModeloTabla(new DefaultTableModel(null, header));
		
		table = new JTable(this.getModeloTabla());
		table.setBounds(0, 0, 532, 195);
		scrollPane.setViewportView(table);
		
		
		
		
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}

	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}
	
	
}
