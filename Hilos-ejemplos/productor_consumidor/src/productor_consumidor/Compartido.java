package productor_consumidor;

import java.util.LinkedList;

public class Compartido {
	LinkedList<Integer> numeros;

	public Compartido() {
		super();
		this.numeros = new LinkedList<Integer>();
	}

	public synchronized void insertar(Integer num) {
		numeros.add(num);
		notifyAll();
	}

	public synchronized Integer retirar() {
		while (getNumeros().isEmpty()) {
			try {
				wait();
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		} 
			return getNumeros().removeLast() ;
		
	}

	public LinkedList<Integer> getNumeros() {
		return numeros;
	}

	public void setNumeros(LinkedList<Integer> numeros) {
		this.numeros = numeros;
	}

}
