package productor_consumidor;

public class Consumidor implements Runnable {
	Compartido compartido;

	public Consumidor(Compartido compartido) {
		super();
		this.compartido = compartido;
	}

	@Override
	public void run() {
		Integer continuar = 0;
		while (continuar <2) {
			Integer num = getCompartido().retirar();
			if (num != null) {
				System.out.println(Thread.currentThread().getName() + ", retiro: " + num);
			}else {
				continuar++;
			}
		}
		System.out.println("finaliza " + Thread.currentThread().getName());

	}

	public Compartido getCompartido() {
		return compartido;
	}

	public void setCompartido(Compartido compartido) {
		this.compartido = compartido;
	}
}
