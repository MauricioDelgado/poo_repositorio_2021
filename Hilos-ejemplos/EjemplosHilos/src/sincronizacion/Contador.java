package sincronizacion;

public class Contador {
	private Integer contador = 0;
	
	public  void incrementar() {
		contador++;
	}
	public  void decrementar() {
		contador--;
	}
	public  Integer getContador() {
		return contador;
	}

}
