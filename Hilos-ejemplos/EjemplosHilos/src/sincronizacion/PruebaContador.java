package sincronizacion;

public class PruebaContador {

	public static void main(String[] args) {
		Contador c = new Contador();
		Thread t1= new Thread(new HiloModificadorContador(c));
		Thread t2= new Thread(new HiloModificadorContador(c));
		
		t1.start();  // Start inicializa la ejecucion del hilo
		t2.start();
		
		try {
			t1.join();
			t2.join();
			System.out.println("al finalizar tengo: "+ c.getContador());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		
	}

}

/*
 * CLASE = Thread  
 * metodo= currentThread --> devuelve el hilo que se ejecuta actualmente --> se le puede poner el "getName()"
 * metodo= start() --> inicializa la ejecucion del hilo (NO CONFUNDIR CON RUN() este ejecuta de manera secuencial)
 * metodo= isAlive() --> True si el hilo esta vivo y no termino su ejecucion
 * 
 * metodo= join() --> hace que espere el tiempo maximo posible, permite a que termine de ejecutarse un hilo
 * metodo= interrupt() --> interrumpe el hilo y avisa al siguiente, se suele usar un join() despues
 * 
 * thread.sleep(int)--> duerme el hilo el tiempo en milisegundos que le indique en los ()
 * thread.wait() --> hace esperar el hilo hasta que se le avise con otro metodo (notifyall)
 * 
 * nombre_hilo.notifyAll() --> "despierta" los hilos que estaban esperando con el wait
 * 
 * system.currentTimeMillis() --> obtiene el tiempo del sistema en milisegundos
 * 
 * --------------------------------------------------------------------------------
 * CLASE QUE HEREDA (EXTENDS) DE THREAD
 * utiliza el metodo RUN()
 * Thread hilo1 = new Hilo1() --> siendo Hilo1 una clase que extiende/hereda de Thread
 * 
 * 
 * CLASE QUE IMPLEMENTA UNA INTERFAZ "RUNNABLE"
 * se utiliza igual el metodo RUN()
 * Thread t2= new Thread(new "CLASE QUE TENGA UN OBJETO DE TIPO HILO" );
 * 
 */