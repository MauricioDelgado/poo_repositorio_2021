
public class Ej2_HolaThread extends Thread {

	public void run() {
		System.out.println("Hola desde un thread!");
		
	}

	public static void main(String args[]) {
		Thread unThread = new Ej2_HolaThread();
		unThread.start();
	}
	

}
/*
Limitado a que se debe heredar de la clase Thread, 
�til para aplicaciones simples. 
La clase Thread define un n�mero de m�todos �tiles
 para el tratamiento de hilos, incluidos m�todos est�ticos, 
 que proveen informaci�n o cambios de estados.
 */