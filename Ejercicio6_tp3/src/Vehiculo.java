
public class Vehiculo {

	private Duenio duenio; 
	private String matricula; 
	private String modelo; 
	private String averia; 
	private Double horasEstimadas; 
	private Double precioRepuesto;
	
	public Vehiculo() {
		this.setDuenio(duenio);
		this.setMatricula(matricula);
		this.setModelo(modelo);
		this.setAveria(averia);
		this.setHorasEstimadas(horasEstimadas);
		this.setPrecioRepuesto(precioRepuesto);
	}
	
	public Vehiculo(Duenio duenio, String matricula, String modelo, String averia, Double horasEstimadas,Double precioRepuesto) {
		this.setDuenio(duenio);
		this.setMatricula(matricula);
		this.setModelo(modelo);
		this.setAveria(averia);
		this.setHorasEstimadas(horasEstimadas);
		this.setPrecioRepuesto(precioRepuesto);
	}

	public Duenio getDuenio() {
		return duenio;
	}

	public void setDuenio(Duenio duenio) {
		this.duenio = duenio;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAveria() {
		return averia;
	}

	public void setAveria(String averia) {
		this.averia = averia;
	}

	public Double getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(Double horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public Double getPrecioRepuesto() {
		return precioRepuesto;
	}

	public void setPrecioRepuesto(Double precioRepuesto) {
		this.precioRepuesto = precioRepuesto;
	} 
	
	
	public Double calcularPrecio() {
		return null; 
	}
	
	public String toString() {
		return "Due�o: " + this.duenio.getNombre()+
				"\nMatricula: "+ this.getMatricula() +
				"\nModelo: "+ this.getModelo()+
				"\nAveria: " + this.getAveria()+
				"\nHora estimada: " + this.getHorasEstimadas()+
				"\nPrecio Repuesto: " + this.getPrecioRepuesto() + "\n"; 
	}
	
}
