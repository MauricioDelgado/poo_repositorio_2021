import java.util.ArrayList;

public class Taller {
	private ArrayList<Vehiculo> vehiculo; 
	private String nombre; 
	private Integer direccion, telefono;
	private Double bomba = 1000.0;
	private Double freno= 2000.0;
	private Double embrague= 1500.0;
	
	public Taller() {
		this.setVehiculo(vehiculo);
		this.setNombre(nombre);
		this.setDireccion(direccion);
		this.setTelefono(telefono);
	}
	
	public Taller(ArrayList<Vehiculo> vehiculo, String nombre, Integer direccion, Integer telefono) {
		super();
		this.setVehiculo(vehiculo);
		this.setNombre(nombre);
		this.setDireccion(direccion);
		this.setTelefono(telefono);
	}


	public ArrayList<Vehiculo> getVehiculo() {
		return vehiculo;
	}


	public void setVehiculo(ArrayList<Vehiculo> vehiculo) {
		this.vehiculo = vehiculo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Integer getDireccion() {
		return direccion;
	}


	public void setDireccion(Integer direccion) {
		this.direccion = direccion;
	}


	public Integer getTelefono() {
		return telefono;
	}


	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}


	public Double getBomba() {
		return bomba;
	}


	public void setBomba(Double bomba) {
		this.bomba = bomba;
	}


	public Double getFreno() {
		return freno;
	}


	public void setFreno(Double freno) {
		this.freno = freno;
	}


	public Double getEmbrague() {
		return embrague;
	}


	public void setEmbrague(Double embrague) {
		this.embrague = embrague;
	} 
	
	public void ingresarCosto() {
		
	}
	
	
}
