import java.util.Scanner;

public class ProgramaPrincipal {

	public static void main(String[] args) {
		
		int numero1, numero2; 
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Escriba el primer numero entero: ");
		numero1 = entrada.nextInt();
		
		System.out.println("Escriba el segundo numero: ");
		numero2 = entrada.nextInt();
		
		int res = numero1 + numero2; 
		System.out.println("La suma es: " + res );
		
		
	}

}
